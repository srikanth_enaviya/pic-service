package Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.List;

import picservice.enaviya.com.picservice.PicLoader;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.TouchImageView;

/**
 * Created by user on 19-09-2017.
 */
public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private List<String> _imagePaths;
    private LayoutInflater inflater;

    // constructor
    public FullScreenImageAdapter(Activity activity,
                                  List<String> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
        try {
            TouchImageView imgDisplay;
            Button btnClose;

            imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
            btnClose = (Button) viewLayout.findViewById(R.id.btnClose);

            /*BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
            imgDisplay.setImageBitmap(bitmap);*/

            int loader = R.mipmap.pleasewait;
                    /*System.out.println("img File : "+imgFile.getAbsolutePath());
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView.setImageBitmap(myBitmap);*/
            PicLoader imgLoader = new PicLoader(_activity);
            imgLoader.DisplayImage(_imagePaths.get(position), loader, imgDisplay);

            // close button click event
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _activity.finish();
                }
            });

            ((ViewPager) container).addView(viewLayout);


        }
        catch (Exception e){
            Log.e("excep",e.getMessage());
        }

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}
