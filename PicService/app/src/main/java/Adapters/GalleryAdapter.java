package Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import Fragments.Home;
import Model.Job_Details;
import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.ImageFullScreen;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 18-09-2017.
 */
public class GalleryAdapter extends ArrayAdapter {
    private static final String TAG = GalleryAdapter.class.getSimpleName();
    public static Home fragment;
    private final Activity context;
    private List<Job_Details> Image;
    DatabaseHandler db;
    private ImageLoader imageLoaderMain;
    private AnimateFirstDisplayListener animateFirstListener1;
    private DisplayImageOptions options;

    public GalleryAdapter(Activity context, List<Job_Details> Image, Home fragment) {
        super(context, R.layout.image_list, Image);
        this.context = context;
        this.Image = Image;
        this.fragment = fragment;
        db = new DatabaseHandler(context);
        imageLoaderMain = ImageLoader.getInstance();
        imageLoaderMain.init(ImageLoaderConfiguration.createDefault(context));

    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.image_list, null, true);
        ImageView ImageView = (ImageView) rowView.findViewById(R.id.imageView1);
        ImageView close = (ImageView) rowView.findViewById(R.id.btnDelete);



        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .resetViewBeforeLoading(true).cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300)).build();
        try {
            //System.out.println("Image URI ::"+Image.get(position));
            File imgFile = new  File(Image.get(position).getImage());

            if(imgFile.exists()){
                try {
                    imageLoaderMain.displayImage("file://"+imgFile.getAbsolutePath(),ImageView,
                            options, animateFirstListener1);

                  /*  Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView.setImageBitmap(myBitmap);*/
                }
                catch (Exception e){
                    Utilities.logError(context, TAG, "onImageView", e.getMessage(), context.getString(R.string.something_went_wrong));
                }

            } else {
                //Toast.makeText(context, "Image Not Found", Toast.LENGTH_SHORT).show();
                //Utilities.showMessageAlertDialog(context, "Image Not Found");
            }
        } catch (Exception e) {
            Utilities.logError(context, TAG, "onImageView", e.getMessage(), context.getString(R.string.something_went_wrong));
            e.printStackTrace();
        }

        ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,ImageFullScreen.class);
                i.putExtra("ImagePath",Image.get(position).getImage());
                context.startActivity(i);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                // set title
                alertDialogBuilder.setTitle("Delete");
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure you want to Delete ?");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DatabaseHandler.deleteRow(context,db.TABLE_GALLERY,db.TABLE_GALLERY_IMAGE_PATH,Image.get(position).getImage());
                        fragment.Show_Grid_Images(Image.get(position).getImageTitle());
                        dialog.cancel();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        return rowView;
    }


    private static class AnimateFirstDisplayListener extends
            SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 0);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}


