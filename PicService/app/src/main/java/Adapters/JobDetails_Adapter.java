package Adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Fragments.Home;
import Model.Job_Details;
import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.MainActivity;
import picservice.enaviya.com.picservice.NetworkConnection;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 18-09-2017.
 */
public class JobDetails_Adapter extends ArrayAdapter {
    private static final String TAG = JobDetails_Adapter.class.getSimpleName();
    String c_light = "c_light.ttf";
    Typeface tf_light;
    LinearLayout rootView;
    public static List<Job_Details> Model;
    private List<Job_Details> suggestions;
    private final Activity context;
    Home home;
    DatabaseHandler db;

    public JobDetails_Adapter(Activity context,List<Job_Details> Model, Home home) {
        super(context, R.layout.job_details_items, Model);
        this.context = context;
        this.Model = Model;
        this.home = home;
        tf_light = Typeface.createFromAsset(context.getAssets(), c_light);
        db =  new DatabaseHandler(context);
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.job_details_items, null, true);
        TextView tvTitle = (TextView) rowView.findViewById(R.id.tvTitle);
        TextView tvDesc = (TextView) rowView.findViewById(R.id.tvDesc);
        TextView tvDate = (TextView) rowView.findViewById(R.id.tvDate);

        ImageView edit_icon = (ImageView) rowView.findViewById(R.id.edit_icon);
        RelativeLayout rootview = (RelativeLayout) rowView.findViewById(R.id.rootview);

        try {
            tvTitle.setText(Model.get(position).getTitle());
            if(Model.get(position).getDesc().contains("|")) {
                String[] parts = Model.get(position).getDesc().split("\\|");
                //tvDesc.setText(parts[0]);
                tvDesc.setText(Model.get(position).getDesc());
            } else {
              tvDesc.setText(Model.get(position).getDesc());
            }
            String str =parseDateToddMMyyyy(Model.get(position).getDate());
            tvDate.setText(str);
          edit_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    (home).ShowDialog(""+position);
                }
            });

           rootview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(NetworkConnection.isNetworkConnected(context)){
                    //System.out.println(Model.get(position).getTitle());
                    ((MainActivity) context).navigateFragments(3, Model.get(position).getTitle());
                    } else {
                        Utilities.showMessageAlertDialog(context, context.getResources().getString(R.string.InternetConnection));
                    }
                }
            });
        } catch (Exception e) {
            Utilities.logError(context, TAG, "onImageView", e.getMessage(), context.getString(R.string.something_went_wrong));
            e.printStackTrace();
        }

        return rowView;
    }


    @Override
    public int getCount() {

        return Model.size();
    }

    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final List<Job_Details> results =  new ArrayList();
                if(suggestions==null){
                    suggestions = Model;
                }

                if (constraint != null) {
                    if (suggestions != null && suggestions.size() > 0) {

                        for (final Job_Details g : suggestions) {
                            if (g.getDesc().toLowerCase().contains(constraint.toString()) || g.getDesc().toUpperCase().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;            }
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                Model = (List<Job_Details>) results.values;
                try {
                    if (Model.size() > 0) {
                        notifyDataSetChanged();

                    } else {
                        notifyDataSetChanged();
                        Toast.makeText(context, "No Records Found", Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException e){
                    System.out.println("Check1");
                }
            }
        };
        return filter;
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd,HH:mm";
        String outputPattern = "MMM dd yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}



