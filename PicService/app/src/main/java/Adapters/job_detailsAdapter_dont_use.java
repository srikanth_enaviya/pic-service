package Adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import Fragments.Home;
import Model.Job_Details;
import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.MainActivity;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 20-10-2017.
 */
public class job_detailsAdapter_dont_use extends RecyclerView.Adapter<job_detailsAdapter_dont_use.ActionListHolder> {
    private static final String TAG = job_detailsAdapter_dont_use.class.getSimpleName();

    String c_light = "c_light.ttf";
    Typeface tf_light;
    LinearLayout rootView;
    private List<Job_Details> Model;
    private final Activity context;
    Home home;
    DatabaseHandler db;

    public class ActionListHolder extends RecyclerView.ViewHolder {

        TextView tvTitle,tvDesc;

        ImageView edit_icon;
        RelativeLayout rootview;

        public ActionListHolder(View view) {
            super(view);
            try {

                tvTitle = (TextView) view.findViewById(R.id.tvTitle);
                tvDesc = (TextView) view.findViewById(R.id.tvDesc);

                edit_icon = (ImageView) view.findViewById(R.id.edit_icon);
                rootview = (RelativeLayout) view.findViewById(R.id.rootview);

            } catch (Exception e) {
                Utilities.logError(context, TAG, "ActionListHolder WeekAdapter", e.getMessage(), context.getString(R.string.something_went_wrong));
            }
        }
    }

    public job_detailsAdapter_dont_use(Activity context,List<Job_Details> Model, Home home) {
        this.context = context;
        this.Model = Model;
        this.home = home;
        tf_light = Typeface.createFromAsset(context.getAssets(), c_light);
        db =  new DatabaseHandler(context);
    }

    @Override
    public ActionListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_details_items, parent, false);
        return new ActionListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ActionListHolder actionListHolder, final int position) {
        try {

            actionListHolder.tvTitle.setText(Model.get(position).getTitle());
            if(Model.get(position).getDesc().contains("|")) {
                String[] parts = Model.get(position).getDesc().split("\\|");
                actionListHolder.tvDesc.setText(parts[0]);
            } else {
                actionListHolder.tvDesc.setText(Model.get(position).getDesc());
            }

            actionListHolder.edit_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    (home).ShowDialog(""+position);
                }
            });

            actionListHolder.rootview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) context).navigateFragments(3, Model.get(position).getTitle());
                }
            });


        } catch (Exception e) {
            Utilities.logError(context, TAG, "onListInteraction Adapter", e.getMessage(), context.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public int getItemCount() {
        return Model.size();
    }

}

