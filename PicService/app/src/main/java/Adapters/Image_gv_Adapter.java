package Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.FullScreenViewActivity;
import picservice.enaviya.com.picservice.PicLoader;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 18-09-2017.
 */
public class Image_gv_Adapter extends ArrayAdapter {
    private static final String TAG = Image_gv_Adapter.class.getSimpleName();
    private final Activity context;
    private List<String> Image;
    DatabaseHandler db;
    private ImageLoader imageLoaderMain;
    private AnimateFirstDisplayListener animateFirstListener1;
    private DisplayImageOptions options;

    public Image_gv_Adapter(Activity context, List<String> Image) {
        super(context, R.layout.image_list1, Image);
        this.context = context;
        this.Image = Image;
        db = new DatabaseHandler(context);
        System.out.println("Adapter Size ::"+Image.size());
       /* imageLoaderMain = ImageLoader.getInstance();
        imageLoaderMain.init(ImageLoaderConfiguration.createDefault(context));*/
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.image_list1, null, true);
        ImageView ImageView = (ImageView) rowView.findViewById(R.id.imageView1);

        try {
            //File imgFile = new  File(Image.get(position));

            //if(imgFile.exists()){
                    if(position == 0){
                        System.out.println("i");
                    }

                try {
                    int loader = R.mipmap.pleasewait;
                    /*System.out.println("img File : "+imgFile.getAbsolutePath());
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    ImageView.setImageBitmap(myBitmap);*/
                    PicLoader imgLoader = new PicLoader(context);
                    imgLoader.DisplayImage(Image.get(position), loader, ImageView);
                }
                catch (Exception e){
                    Utilities.logError(context, TAG, "onImageView", e.getMessage(), context.getString(R.string.something_went_wrong));
                }
           /* } else {
                Utilities.showMessageAlertDialog(context, "Image Not Found");
            }*/
        } catch (Exception e) {
            Utilities.logError(context, TAG, "onImageView", e.getMessage(), context.getString(R.string.something_went_wrong));
            e.printStackTrace();
        }

        ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, FullScreenViewActivity.class);
                i.putExtra("position", ""+position);
                context.startActivity(i);
            }
        });

        return rowView;
    }


    private static class AnimateFirstDisplayListener extends
            SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 0);
                    displayedImages.add(imageUri);
                }
            }
        }

    }

}



