package ApiResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 18-09-2017.
 */
public class RegisterResponse {
    @SerializedName("responseCode")
    @Expose
    private String responseCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("userDTO")
    @Expose
    private UserDTO userDTO;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }



    public class Roles {

        @SerializedName("idUserRoles")
        @Expose
        private Integer idUserRoles;
        @SerializedName("userType")
        @Expose
        private String userType;

        public Integer getIdUserRoles() {
            return idUserRoles;
        }

        public void setIdUserRoles(Integer idUserRoles) {
            this.idUserRoles = idUserRoles;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

    }



    public class UserDTO {

        @SerializedName("idUser")
        @Expose
        private Integer idUser;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("userPassword")
        @Expose
        private String userPassword;
        @SerializedName("enabled")
        @Expose
        private Boolean enabled;
        @SerializedName("userDetails")
        @Expose
        private Object userDetails;
        @SerializedName("roles")
        @Expose
        private Roles roles;

        public Integer getIdUser() {
            return idUser;
        }

        public void setIdUser(Integer idUser) {
            this.idUser = idUser;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserPassword() {
            return userPassword;
        }

        public void setUserPassword(String userPassword) {
            this.userPassword = userPassword;
        }

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public Object getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(Object userDetails) {
            this.userDetails = userDetails;
        }

        public Roles getRoles() {
            return roles;
        }

        public void setRoles(Roles roles) {
            this.roles = roles;
        }

    }

}
