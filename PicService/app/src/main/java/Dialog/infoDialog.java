package Dialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import picservice.enaviya.com.picservice.MainActivity;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 16-10-2017.
 */
public class infoDialog extends DialogFragment {

    TextView close;
    private MainActivity parentActivity;

    /*
     * (non-Javadoc)
     * @see android.app.DialogFragment#onCreateDialog(android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater layoutInfaltor,
                             ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = layoutInfaltor.inflate(R.layout.info_dialog,
                viewGroup, false);
        try {
            close = (TextView) view.findViewById(R.id.close);

            getDialog().setCancelable(false);
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });


        } catch (Exception e){
            Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.something_went_wrong));
        }
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        parentActivity = (MainActivity) activity;
    }

}

