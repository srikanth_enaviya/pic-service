package backend_service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ApiRequest.Register;
import ApiRequest.req_api;
import api.BaseBuilder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.MultipartRequest;
import picservice.enaviya.com.picservice.NetworkConnection;
import picservice.enaviya.com.picservice.SessionManager;
import picservice.enaviya.com.picservice.VolleySingleton;

/**
 * Created by user on 05-12-2017.
 */
public class Offline_Images extends Service {

    private Handler handler;
    private Runnable myRunnable;
    SessionManager Session;
    DatabaseHandler db;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    String Title="",Desc ="";
    List<String> Id_ImagePath = new ArrayList();
    List<String> Id_ImagePath1 = new ArrayList();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        try {

            super.onCreate();
        } catch (Exception e){
            //Log.e(TAG, " Oncreate() : " + e.getMessage());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        myRunnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 5000);
                if(NetworkConnection.isNetworkConnected1(Offline_Images.this)){
                    db = new DatabaseHandler(Offline_Images.this);
                    Session = new SessionManager(Offline_Images.this);
                    List<String[]> ImgDetails =  new ArrayList();
                    ImgDetails = db.getQueryData(Offline_Images.this,
                            "select * from " + db.TABLE_OFFLINE);
                    if(ImgDetails.size() > 0){
                        for(int i=0; i < ImgDetails.size();i++){
                            CALL_ADDJOB_API(ImgDetails.get(i)[1],ImgDetails.get(i)[2],ImgDetails.get(i)[3],ImgDetails.get(i)[4],ImgDetails.get(i)[5],ImgDetails.get(i)[6],ImgDetails.get(i)[7]);
                        }
                        db.deleteTable(Offline_Images.this,db.TABLE_OFFLINE);
                    } else {
                        Call_Update_API();
                    }
                }
            }
        };
        handler.postDelayed(myRunnable, 60000);
        return Service.START_NOT_STICKY;
    }

    private void Call_Update_API() {

        try {
            Id_ImagePath.clear();
            Id_ImagePath1.clear();
            List<String[]> ImagePath =  new ArrayList();
            ImagePath = db.getQueryData(Offline_Images.this,
                    "select * from " + db.TABLE_OFF_GALLERY +" where "+db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE);

            List<String[]> ImagePath1 =  new ArrayList();
            ImagePath1 = db.getQueryData(Offline_Images.this,
                    "select * from " + db.TABLE_GALLERY +" where "+db.TABLE_GALLERY_IMAGE_JOBTITLE);


            String values = "";
            if(ImagePath.size()>0){

                for(int i= 0; i < ImagePath.size(); i++){

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
                    String formattedDate = df.format(c.getTime());

                    req_api ra = new req_api();
                    req_api.IdProject rId = new req_api().new IdProject();

                    ra.setId(0);
                    ra.setDescription(ImagePath.get(i)[1]);
                    ra.setContenttype("PNG");
                    ra.setCreatedDate(formattedDate);
                    rId.setProjectTitle(ImagePath.get(i)[4]);
                    Title = ImagePath.get(i)[4];
                    ra.setIdProject(rId);
                    values = new Gson().toJson(ra);

                    //System.out.println("Image PAth size :: "+ImagePath.size());
                    Id_ImagePath.add(ImagePath.get(i)[2]);

                }
            } else {
                for(int i= 0; i < ImagePath1.size(); i++){

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
                    String formattedDate = df.format(c.getTime());

                    req_api ra = new req_api();
                    req_api.IdProject rId = new req_api().new IdProject();

                    ra.setId(0);
                    ra.setDescription(ImagePath1.get(i)[1]);
                    ra.setContenttype("PNG");
                    ra.setCreatedDate(formattedDate);
                    rId.setProjectTitle(ImagePath1.get(i)[4]);
                    Title = ImagePath1.get(i)[4];
                    ra.setIdProject(rId);
                    values = new Gson().toJson(ra);

                    //System.out.println("Image PAth size :: "+ImagePath.size());
                    Id_ImagePath1.add(ImagePath.get(i)[2]);

                }

            }

            if(ImagePath.size() > 0) {
                CallHandler(values, Id_ImagePath, Title);
            }
            if(ImagePath1.size()>0){
                CallHandler(values, Id_ImagePath1, Title);
            }


        }
        catch (Exception e){

        }
    }


    private void CALL_ADDJOB_API(String title, String desc, String Client,String ref, String vessel, String comm, String stokpile) {
        try {
            Calendar c = Calendar.getInstance();
            //System.out.println("Current time => "+c.getTime());
            Title = title;
            Desc = desc;

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
            String formattedDate = df.format(c.getTime());

            Register register = new Register();
            Register.IdUser reg_user = new Register().new IdUser();

            register.setIdProject("0");
            register.setProjectTitle(title);
            register.setProjectDescription(desc+" | "+ Client + " | " + ref+" | " + vessel +" | " +comm + " | " + stokpile);
            register.setDateAdded(formattedDate);

            reg_user.setUserName(Session.getUsername());
            register.setIdUser(reg_user);

            //System.out.println("Parameters : "+new Gson().toJson(register));

            ProjectAuthHandler projectAuthHandler = new ProjectAuthHandler();
            projectAuthHandler.execute(BaseBuilder.Projects, new Gson().toJson(register));


        } catch (Exception e){
            Log.e("CALL_ADDJOB_API",e.getMessage());
        }
    }


    public class ProjectAuthHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            RequestBody body = RequestBody.create(JSON, params[1]);
            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.post(body);
            builder.addHeader("Authorization",Session.getAuth());
            Request request = builder.build();

            System.out.println("URl :" +params[0]);
            System.out.println("body :" +params[1]);

            try {
                okhttp3.Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            System.out.println("project details :: "+res);
            Id_ImagePath.clear();
            try {
                JSONObject jObj = new JSONObject(res);
                String Proj_Title = jObj.getString("projectTitle");
                if(Proj_Title.equalsIgnoreCase(Title)){
                    Toast.makeText(Offline_Images.this, "Offline Project added successfully!", Toast.LENGTH_SHORT).show();
                    System.out.println("Project added : "+Title);

                    Id_ImagePath.clear();
                    List<String[]> ImagePath =  new ArrayList();
                    ImagePath = db.getQueryData(Offline_Images.this,
                            "select * from " + db.TABLE_OFF_GALLERY +" where "+db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE+ " = '"+Title+"'");

                    String values = "";
                    for(int i= 0; i < ImagePath.size(); i++){

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
                        String formattedDate = df.format(c.getTime());

                        req_api ra = new req_api();
                        req_api.IdProject rId = new req_api().new IdProject();

                        ra.setId(0);
                        ra.setDescription(ImagePath.get(i)[1]);
                        ra.setContenttype("PNG");
                        ra.setCreatedDate(formattedDate);
                        rId.setProjectTitle(ImagePath.get(i)[4]);
                        ra.setIdProject(rId);
                        values = new Gson().toJson(ra);

                        //System.out.println("Image PAth size :: "+ImagePath.size());
                        Id_ImagePath.add(ImagePath.get(i)[2]);

                    }
                    if(ImagePath.size() > 0) {
                        CallHandler(values, Id_ImagePath, Title);
                    }
                    //db = new DatabaseHandler(Offline_Images.this);
                    // check once

                } else {

                }

            } catch (Exception e){
                Log.e("on failed adding:",e.getMessage());
            }

        }
    }


    private void CallHandler(String values, final List<String> Imagepath, final String title) {
        try{

            System.out.println("file here :" +Session.getAuth());

            MultipartRequest mreq = new MultipartRequest(BaseBuilder.Documents,new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("Error :"," error.toString "+error.toString());
                }},new com.android.volley.Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Toast.makeText(Offline_Images.this, "Offline Image Upload successfully!", Toast.LENGTH_SHORT).show();
                        Log.i("Rsponse "," resultResponse "+response);
                        //for(int i =0; i< Imagepath.size(); i++){
                        DatabaseHandler.deleteRows(Offline_Images.this, DatabaseHandler.TABLE_OFF_GALLERY,
                                DatabaseHandler.TABLE_OFF_GALLERY_IMAGE_JOBTITLE, title);

                    } catch (Exception e){
                        Log.i("Error1 :"," error.toString "+e.getMessage());
                    }
                }
            },Imagepath,values,Session.getAuth(),Offline_Images.this);

            mreq.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 120000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 120000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {
                    Log.i("Error :"," error.toString "+error.toString());
                }
            });

            VolleySingleton.getInstance(Offline_Images.this).addToRequestQueue(mreq);
            //}
        }
        catch (Exception e){
            Log.e("CallHandler ",e.getMessage());
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }
}
