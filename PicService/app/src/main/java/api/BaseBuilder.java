package api;

import android.content.Context;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by user on 18-09-2017.
 */
public class BaseBuilder {
    public static final String BASE_URL = "http://192.157.238.98:4001/picservice/";


    public static final String Login = BASE_URL+"appauth/login";
    public static final String Forgot = BASE_URL+"appverifier/passwordresetauthorization";
    public static final String Refresh_Token = BASE_URL+"appverifier/login";
    public static final String UsersList = BASE_URL+"appauth/users";
    public static final String JOB_Details = BASE_URL+"projects/user/";
    public static final String Projects = BASE_URL+"projects/";
    public static final String Documents = BASE_URL+"documents/";
    public static final String Image_ID_Details = BASE_URL+"documents/project/";
    public static final String Image_Details = BASE_URL+"documents/downloads/";
    //public static final String Image_Details_browser = BASE_URL+"documents/downloadImage/";
    public static final String Image_Details_browser = BASE_URL+"documents/downloads/";



    public Retrofit getRetrofitInstance(String baseURL, Gson customGsonParser, Context context) {


        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        OkHttpClient shortHttpClient = httpBuilder.readTimeout(60, SECONDS)
                .writeTimeout(60, SECONDS)
                .connectTimeout(60, SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl((baseURL == null) ? BASE_URL : baseURL)
                .addConverterFactory((customGsonParser == null) ? GsonConverterFactory.create() : GsonConverterFactory.create(customGsonParser))
                .client(shortHttpClient)
                .build();

    }
}

