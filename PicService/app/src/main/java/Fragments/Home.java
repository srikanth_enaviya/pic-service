package Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import Adapters.GalleryAdapter;
import Adapters.JobDetails_Adapter;
import ApiRequest.Register;
import ApiRequest.req_api;
import Model.Job_Details;
import api.BaseBuilder;
import backend_service.Offline_Images;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.MultipartRequest;
import picservice.enaviya.com.picservice.NetworkConnection;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.SessionManager;
import picservice.enaviya.com.picservice.Utilities;
import picservice.enaviya.com.picservice.VolleySingleton;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by user on 18-09-2017.
 */
public class Home extends Fragment implements SearchView.OnQueryTextListener {
    private static final String TAG = Home.class.getSimpleName();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final int PERMISSIONS_REQUEST_GALLERY_CODE = 101;
    private static final int TAKE_PICTURE = 1;
    public static Activity activity;
    public static Home homefragment;
    private SessionManager Session;
    public static DatabaseHandler db;
    //RecyclerView job_list;
    Button CreateBtn;
    public static RelativeLayout image_layout;
    public static  Uri imageUri;
    String ImageName="",imagePath ="",information;
    public static List<Job_Details> Image_Uri = new ArrayList();
    public static GridView gridView;
    public static ListView job_list;
    JobDetails_Adapter jobDetails_Adapter;
    SearchView search;

    RelativeLayout empty_layout;

    List<Job_Details> Details = new ArrayList<>();
    EditText edJobTitle,edJobDesc,edClient,edbref,edvessel,edCommodity,edstokpile;
    TextView title_tv;
    Dialog dialog,Spinnerdialog;


    List<String> Id_ImagePath = new ArrayList();
    int idSize = 0,spinning =0;
    String update ="false";

    public Home() {
        // Required empty public constructor
    }

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState){
        View fragment = null;
        try {


            fragment = inflater.inflate(R.layout.fragment_home, container, false);
            homefragment = new Home();
            activity = (Activity)fragment.getContext();
            Spinnerdialog = new Dialog(getActivity());
            Bundle bundle = this.getArguments();
            information = bundle.getString("Information");
            Session = new SessionManager(getActivity());
            db =  new DatabaseHandler(getActivity());
            job_list = (ListView) fragment.findViewById(R.id.job_list);
            CreateBtn = (Button) fragment.findViewById(R.id.CreateBtn);
            empty_layout = (RelativeLayout) fragment.findViewById(R.id.empty_layout);
            search = (SearchView) fragment.findViewById(R.id.search);

            //DeleteDIR();
            CreateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowDialog("");
                }
            });

            if(information.equalsIgnoreCase("create")){
                ShowDialog("");
            }

            GetJobDetails();
            setupSearchView();
            CheckService();


        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "onCreateView", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
        return fragment;
    }

    private void CheckService() {
        try {

            getActivity().startService(new Intent(getActivity(), Offline_Images.class));
          
        }
        catch (Exception e){

        }
    }


    private void setupSearchView() {
        search.setIconifiedByDefault(false);
        search.setOnQueryTextListener(this);
        search.setSubmitButtonEnabled(false);
        search.setQueryHint("Search");
    }

    public void GetJobDetails() {
        try {
           /* db.deleteTable(getActivity(),db.TABLE_GALLERY);
            db.deleteTable(getActivity(),db.TABLE_JOB);*/

            if(!information.equalsIgnoreCase("create")){
                check_dialog();
            }

            if(NetworkConnection.isNetworkConnected(getActivity())){
                JobAuthHandler jobAuthHandler = new JobAuthHandler();
                jobAuthHandler.execute(BaseBuilder.JOB_Details+Session.getUsername());
            } else {
                Spinnerdialog.dismiss();
                Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.InternetConnection));
            }

        }
        catch (Exception e) {
            Spinnerdialog.dismiss();
            Log.e("GetJobDetails",e.getMessage());
            Utilities.logError(getActivity(), TAG, "GetJobDetails", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    private void DeleteDIR() {
        try {
            File dir = new File(Environment.getExternalStorageDirectory(), "/PicService");
            if (dir.isDirectory())
            {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++)
                {
                    new File(dir, children[i]).delete();
                }
            }
        }
        catch (Exception e){

        }
    }


    public class JobAuthHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.addHeader("Authorization",Session.getAuth());
            Request request = builder.build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                }
            });


            try {
                okhttp3.Response response = client.newCall(request).execute();
                return response.body().string();
            }
            catch (Exception e){
                Spinnerdialog.dismiss();
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            System.out.println("Job Response : "+res);
            try {
                Details.clear();
                JSONArray arr = new JSONArray(res);
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject job_details = arr.getJSONObject(i);
                    Job_Details JD = new Job_Details();
                    JD.setSno(job_details.getString("idProject"));
                    JD.setTitle(job_details.getString("projectTitle"));
                    JD.setDesc(job_details.getString("projectDescription"));
                    JD.setDate(job_details.getString("dateAdded"));
                    Details.add(JD);
                }
                Collections.reverse(Details);

                jobDetails_Adapter = new JobDetails_Adapter(getActivity(), Details,Home.this);
              /*  RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(),2);
                job_list.setLayoutManager(mLayoutManager);
                job_list.setItemAnimator(new DefaultItemAnimator());*/
                job_list.setAdapter(jobDetails_Adapter);

                //System.out.println("JobDetails Size : "+JobDetails.size());
                if(Details.size() <=0){
                    Session.newUser("yes");
                    empty_layout.setVisibility(View.VISIBLE);
                } else {
                    Session.newUser("no");
                    empty_layout.setVisibility(View.GONE);
                }
                Spinnerdialog.dismiss();

            } catch (Exception e) {
                Spinnerdialog.dismiss();
                String status ="";
                Log.e("JobAuthHandler" , e.getMessage());
                try {
                    JSONObject s = new JSONObject(res);
                    status = s.getString("status");
                } catch (Exception e1){
                    Log.e("JobAuthHandler",e.getMessage());
                }
                if(status.equalsIgnoreCase("403")){
                    JSONObject Login = new JSONObject();
                    try {
                        Login.put("token", Session.getRefresh());
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    LoginAuthHandler loginAuthHandler = new LoginAuthHandler();
                    loginAuthHandler.execute(BaseBuilder.Refresh_Token, String.valueOf(Login));
                    //Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.auth_expired));
                } else if(status.equalsIgnoreCase("500")) {
                    Utilities.showMessageAlertDialog(getActivity(), e.getMessage());
                } else {
                    if(isAdded()){
                        Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.something_went_wrong));
                    }
                }
            }

        }
    }



    public class LoginAuthHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            RequestBody body = RequestBody.create(JSON, params[1]);
            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.post(body);
            Request request = builder.build();

            System.out.println("URl :" +params[0]);
            System.out.println("body :" +params[1]);

            try {
                okhttp3.Response response = client.newCall(request).execute();
                String response_auth = response.headers().get("authorization")+"-"+response.headers().get("refresh")+"-"+response.body().toString();
                return response_auth;
            }catch (Exception e){
                dialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                super.onPostExecute(res);
                System.out.println("Refresh Response : "+res);
                String[] parts = res.split("-");

                if (parts[0].equalsIgnoreCase(null) || parts[0] == null || parts[0].equalsIgnoreCase("null")) {
                    //dialog.dismiss();
                    Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.something_went_wrong1));
                } else if (parts[0].equalsIgnoreCase("")) {
                    //dialog.dismiss();
                    Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.something_went_wrong));
                } else {
                    Session.Auth(parts[0],parts[1]);
                    GetJobDetails();
                }
            } catch (Exception e){
                //dialog.dismiss();
                Log.e(TAG,e.getMessage());
                Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.invalidCredentials));
            }
        }
    }

    public void ShowDialog(final String position) {
        try {

            db.deleteTable(getActivity(),db.TABLE_GALLERY);
            File dir = new File(Environment.getExternalStorageDirectory(), "/PicService");
            if (dir.isDirectory())
            {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++)
                {
                    new File(dir, children[i]).delete();
                }
            }
            TextView more_close;
            Button Uploadimage,submitBtn;


            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.fragment_job_create_new);
            dialog.setCancelable(true);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.gravity = Gravity.CENTER;
//This makes the dialog take up the full width
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            edJobTitle = (EditText) dialog.findViewById(R.id.edJobTitle);
            edJobDesc = (EditText) dialog.findViewById(R.id.edJobDesc);
            edClient = (EditText) dialog.findViewById(R.id.edClient);
            edbref = (EditText) dialog.findViewById(R.id.edbref);
            edvessel = (EditText) dialog.findViewById(R.id.edvessel);
            edCommodity = (EditText) dialog.findViewById(R.id.edCommodity);
            edstokpile = (EditText) dialog.findViewById(R.id.edstokpile);
            title_tv = (TextView) dialog.findViewById(R.id.title_tv);
            more_close = (TextView) dialog.findViewById(R.id.more_close);
            Uploadimage = (Button) dialog.findViewById(R.id.UploadBtn);
            submitBtn = (Button) dialog.findViewById(R.id.submitBtn);
            gridView = (GridView) dialog.findViewById(R.id.gridView);
            image_layout = (RelativeLayout) dialog.findViewById(R.id.image_layout);

            //dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST);

            dialog.show();

            if(!position.equalsIgnoreCase("")){
                title_tv.setText("Edit");
                String desc ="",client= "",bref="",vessel="",commodity="",stokpile="";
                edJobTitle.setText(JobDetails_Adapter.Model.get(Integer.parseInt(position)).getTitle());
                if(JobDetails_Adapter.Model.get(Integer.parseInt(position)).getDesc().contains("|")){
                    System.out.println("Desc: "+JobDetails_Adapter.Model.get(Integer.parseInt(position)).getDesc());
                    String[] parts = JobDetails_Adapter.Model.get(Integer.parseInt(position)).getDesc().split("\\|");
                    desc = parts[0]; client=parts[1]; bref = parts[2]; vessel = parts[3]; commodity = parts[4]; stokpile = parts[5];

                } else {
                    desc = JobDetails_Adapter.Model.get(Integer.parseInt(position)).getDesc();
                }
                edJobDesc.setText(desc);
                edClient.setText(client);
                edbref.setText(bref);
                edvessel.setText(vessel);
                edCommodity.setText(commodity);
                edstokpile.setText(stokpile);

                edJobTitle.setEnabled(false); edClient.setEnabled(false); edbref.setEnabled(false); edvessel.setEnabled(false); edCommodity.setEnabled(false); edstokpile.setEnabled(false);

                imagePath = Details.get(Integer.parseInt(position)).getImageTitle();
                ImageName = imagePath;
                Show_Grid_Images(imagePath);

            } else {
                title_tv.setText("Create");
                ImageName = String.valueOf(System.currentTimeMillis());
                Show_Grid_Images("no");
            }

            more_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(NetworkConnection.isNetworkConnected(getActivity())){
                        DeleteDIR();
                        db.deleteTable(getActivity(),db.TABLE_GALLERY);
                        //GetJobDetails();
                    } else {
                        List<String[]> ImgDetails =  new ArrayList();
                        try {
                            ImgDetails = db.getQueryData(activity,
                                    "select * from " + db.TABLE_OFF_GALLERY + " where "+db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE+ " = '"+edJobTitle.getText().toString().trim()+"'");

                            System.out.println("close "+ImgDetails.size());

                            for(int i =0; i< ImgDetails.size();i++){
                                System.out.println("close deleted ");
                                DatabaseHandler.deleteRows(getActivity(), DatabaseHandler.TABLE_OFF_GALLERY,
                                        DatabaseHandler.TABLE_OFF_GALLERY_IMAGE_JOBTITLE, edJobTitle.getText().toString().trim());
                            }
                        } catch (Exception e){
                            Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
                        }

                    }

                    //GetJobDetails();
                }
            });

            Uploadimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(position.equalsIgnoreCase("")) {
                        if (edJobTitle.getText().toString().trim().equalsIgnoreCase("") || edJobDesc.getText().toString().trim().equalsIgnoreCase("") || edClient.getText().toString().trim().equalsIgnoreCase("") || edbref.getText().toString().trim().equalsIgnoreCase("") || edvessel.getText().toString().trim().equalsIgnoreCase("") || edCommodity.getText().toString().trim().equalsIgnoreCase("") || edstokpile.getText().toString().trim().equalsIgnoreCase("")) {
                            Utilities.showMessageAlertDialog(getActivity(), "Please fill all the fields to upload an image");
                        } else {
                            gallerySelection();
                        }
                    } else {
                        gallerySelection();
                    }
                }
            });


            submitBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        //System.out.println("file Absolute path : : "+ImageName);
                        List<String[]> ImgDetails =  new ArrayList();
                        if (position.equalsIgnoreCase("")){
                            if(NetworkConnection.isNetworkConnected(getActivity())){
                                ImgDetails = db.getQueryData(getActivity(),
                                        "select * from " + db.TABLE_GALLERY + " where "+db.TABLE_GALLERY_IMAGE_NAME+ " = "+ImageName);

                            } else {
                                ImgDetails = db.getQueryData(getActivity(),
                                        "select * from " + db.TABLE_OFF_GALLERY + " where "+db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE+ " = '"+edJobTitle.getText().toString().trim()+"'");

                            }

                            if (edJobTitle.getText().toString().trim().equalsIgnoreCase("") || edJobDesc.getText().toString().trim().equalsIgnoreCase("") || edClient.getText().toString().trim().equalsIgnoreCase("") || edbref.getText().toString().trim().equalsIgnoreCase("") || edvessel.getText().toString().trim().equalsIgnoreCase("") || edCommodity.getText().toString().trim().equalsIgnoreCase("") || edstokpile.getText().toString().trim().equalsIgnoreCase("") || ImageName.equalsIgnoreCase("")) {
                                Utilities.showMessageAlertDialog(getActivity(), "Please fill all the details !");
                            } else if(ImgDetails.size() == 0 && position.equalsIgnoreCase("")){
                                Utilities.showMessageAlertDialog(getActivity(), "Upload an Image");
                            } else {
                                if(NetworkConnection.isNetworkConnected(getActivity())){
                                    CALL_ADDJOB_API();
                                } else {
                                    if(Session.getnewUser().equalsIgnoreCase("yes")){
                                        Utilities.showMessageAlertDialog(getActivity(), "Check your Internet Connection");
                                    } else {
                                        OfflineDB();
                                    }


                                }
                            }

                        } else {
                            System.out.println(edJobDesc.getText().toString().trim()+":desc:: title:"+edJobTitle.getText().toString().trim());
                            List<String[]> ImagePath =  new ArrayList();
                            ImagePath = db.getQueryData(getActivity(),
                                    "select * from " + db.TABLE_GALLERY);

                            if(edJobDesc.getText().toString().trim().equalsIgnoreCase("")){
                                Utilities.showMessageAlertDialog(getActivity(), "Please fill all fields");
                            } /*else if(ImagePath.size() == 0) {
                                Utilities.showMessageAlertDialog(getActivity(), "Upload an Image");
                            } */else {
                                Id_ImagePath.clear();
                                check_dialog();
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
                                String formattedDate = df.format(c.getTime());

                                req_api ra = new req_api();
                                req_api.IdProject rId = new req_api().new IdProject();

                                ra.setId(0);
                                ra.setDescription(edJobDesc.getText().toString().trim());
                                ra.setContenttype("PNG");
                                ra.setCreatedDate(formattedDate);
                                rId.setProjectTitle(edJobTitle.getText().toString().trim());
                                ra.setIdProject(rId);
                                String values = new Gson().toJson(ra);

                                System.out.println("Image PAth size :: " + ImagePath.size());
                                for (int i = 0; i < ImagePath.size(); i++) {
                                    File photo = new File(ImagePath.get(i)[2]);
                                    File newFile = new File(String.valueOf(System.currentTimeMillis()));
                                    photo.renameTo(newFile);
                                    Id_ImagePath.add(ImagePath.get(i)[2]);
                                    System.out.println("Image PAth :: " + ImagePath.get(i)[2]);
                                }
                                update = "true";
                                if(NetworkConnection.isNetworkConnected(getActivity())){
                                    CallHandler(values, Id_ImagePath);
                                } else {
                                    Spinnerdialog.dismiss();
                                    dialog.dismiss();
                                    Utilities.showMessageAlertDialog(getActivity(),"Saved offline temporarily");
                                }

                            }

                        }
                    } catch (Exception e){
                        Spinnerdialog.dismiss();
                        Utilities.logError(getActivity(), TAG, "submitBtn", e.getMessage(), getResources().getString(R.string.something_went_wrong));
                    }
                }
            });

        }
        catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "ShowDialog", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    private void OfflineDB() {
        try{
            boolean bool = db.Checkdata1(getActivity(),
                    db.TABLE_OFFLINE, edJobTitle.getText().toString().trim(), db.OFFLINE_TITLE);
            if(bool){
                Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.proj_title));
            } else {
                db.insertintoTable(getActivity(),
                        db.TABLE_OFFLINE, new String[]{
                                db.OFFLINE_TITLE,
                                db.OFFLINE_DESC,
                                db.OFFLINE_CLIENT_NAME, db.OFFLINE_REF_NAME, db.OFFLINE_VESSEL_NAME,
                                db.OFFLINE_COMMODITY_NAME, db.OFFLINE_STOKPILE_NAME
                        },
                        new String[]{edJobTitle.getText().toString().trim(), edJobDesc.getText().toString().trim(), edClient.getText().toString().trim(),
                                edbref.getText().toString().trim(), edvessel.getText().toString().trim(), edCommodity.getText().toString().trim(),
                                edstokpile.getText().toString().trim()});

                getActivity().startService(new Intent(getActivity(), Offline_Images.class));
                dialog.dismiss();
                Utilities.showMessageAlertDialog(getActivity(),"Saved offline temporarily");
                //GetJobDetails();
            }

        }
        catch (Exception e){
            Utilities.logError(getActivity(), TAG, "OfflineDB", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    private void CALL_ADDJOB_API() {
        try {
            check_dialog();
            Calendar c = Calendar.getInstance();
            //System.out.println("Current time => "+c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
            String formattedDate = df.format(c.getTime());

            Register register = new Register();
            Register.IdUser reg_user = new Register().new IdUser();

            register.setIdProject("0");
            register.setProjectTitle(edJobTitle.getText().toString().trim());
            register.setProjectDescription(edJobDesc.getText().toString().trim()+" | "+ edClient.getText().toString().trim() + " | " + edbref.getText().toString().trim() +" | " + edvessel.getText().toString().trim() +" | " +edCommodity.getText().toString().trim() + " | " + edstokpile.getText().toString().trim());
            register.setDateAdded(formattedDate);

            reg_user.setUserName(Session.getUsername());
            register.setIdUser(reg_user);

            //System.out.println("Parameters : "+new Gson().toJson(register));


            ProjectAuthHandler projectAuthHandler = new ProjectAuthHandler();
            projectAuthHandler.execute(BaseBuilder.Projects, new Gson().toJson(register));


        } catch (Exception e){
            Spinnerdialog.dismiss();
            Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
        }
    }

    public static void Show_Off_Grid_Images(String name) {
        List<String[]> ImgDetails =  new ArrayList();
        List<String[]> ImgDetails1 =  new ArrayList();

        ImgDetails1 = db.getQueryData(activity,
                "select * from " + db.TABLE_OFF_GALLERY);

        System.out.println("off_2:"+ImgDetails1.size());

            ImgDetails = db.getQueryData(activity,
                    "select * from " + db.TABLE_OFF_GALLERY + " where "+db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE+ " = '"+name+"'");

        System.out.println("off_2:"+ImgDetails);


        Image_Uri.clear();
        for(int i =0; i< ImgDetails.size();i++){
            Job_Details JD = new Job_Details();
            JD.setImage(ImgDetails.get(i)[2]);
            JD.setImageTitle(ImgDetails.get(i)[3]);
            Image_Uri.add(JD);
        }
        if(ImgDetails.size() > 0){
            image_layout.setVisibility(View.VISIBLE);
        } else {
            image_layout.setVisibility(View.GONE);
        }
        GalleryAdapter adapter = new GalleryAdapter(activity, Image_Uri,homefragment);

        gridView.setAdapter(adapter);
    }

    public static void Show_Grid_Images(String position) {
        try {
            List<String[]> ImgDetails =  new ArrayList();
            if(position.equalsIgnoreCase("no")){
                ImgDetails = db.getQueryData(activity,
                        "select * from " + db.TABLE_GALLERY + " where "+db.TABLE_GALLERY_IMAGE_NAME+ " = "+position);
            } else {
                ImgDetails = db.getQueryData(activity,
                        "select * from " + db.TABLE_GALLERY);
            }

            Image_Uri.clear();
            for(int i =0; i< ImgDetails.size();i++){
                Job_Details JD = new Job_Details();
                JD.setImage(ImgDetails.get(i)[2]);
                JD.setImageTitle(ImgDetails.get(i)[3]);
                Image_Uri.add(JD);
            }
            if(ImgDetails.size() > 0){
                image_layout.setVisibility(View.VISIBLE);
            } else {
                image_layout.setVisibility(View.GONE);
            }
            GalleryAdapter adapter = new GalleryAdapter(activity, Image_Uri,homefragment);

            gridView.setAdapter(adapter);
        }
        catch (Exception e){
            Log.e("Exception Images", e.getMessage());
        }
    }

    private void gallerySelection() {
        try {
            final CharSequence[] items = {"Choose from Library","Take Photo", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Profile Picture!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Choose from Library")) {
                        openGallery();
                    } else  if (items[item].equals("Take Photo")) {
                        takePhoto();
                    }else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "gallerySelection", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    private void openGallery() {
        try {
            Log.i(TAG, "openGallery");
            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PERMISSIONS_REQUEST_GALLERY_CODE);
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                //intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PERMISSIONS_REQUEST_GALLERY_CODE);
            }
        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "openGallery", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    private void takePhoto() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //imageUri = Uri.fromFile(getOutputMediaFile());
        imageUri = FileProvider.getUriForFile(getActivity(),
                getActivity().getApplicationContext().getPackageName() + ".provider",
                getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);


   /*     Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String ImageTitle = String.valueOf(System.currentTimeMillis());
        File photo = new File(Environment.getExternalStorageDirectory(),  ImageTitle);
           intent.putExtra(MediaStore.EXTRA_OUTPUT,
                   FileProvider.getUriForFile(getActivity(),
                           getActivity().getApplicationContext().getPackageName() + ".provider",
                           photo));
        imageUri = FileProvider.getUriForFile(getActivity(),
                getActivity().getApplicationContext().getPackageName() + ".provider",
                        photo);*/

       /* String authorities = getActivity().getApplicationContext().getPackageName() + ".fileprovider";
        Uri imageUri = FileProvider.getUriForFile(getActivity(), authorities, photo);
        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);*/
     /*   intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        imageUri = Uri.fromFile(photo);*/
        //System.out.println("imageUri action plan :: "+imageUri);
        startActivityForResult(intent, TAKE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK) {
                if (requestCode == PERMISSIONS_REQUEST_GALLERY_CODE) {
                    if (data != null) {
                        Uri selectedImageUri = data.getData();
                        imageUri = selectedImageUri;
                        imagePath = getPath( getActivity(), imageUri );
                        if (imagePath.isEmpty()) {
                            Utilities.showMessageAlertDialog(getActivity(), "Unable to get the selected image");
                        } else {
                            File f = new File(imagePath);
                            moveFile(f.getParent(),f.getName());
                            if(NetworkConnection.isNetworkConnected(getActivity())){
                                db.insertintoTable(getActivity(),
                                        db.TABLE_GALLERY, new String[]{
                                                db.TABLE_GALLERY_IMAGE_DESC,
                                                db.TABLE_GALLERY_IMAGE_PATH,
                                                db.TABLE_GALLERY_IMAGE_NAME,
                                                db.TABLE_GALLERY_IMAGE_JOBTITLE
                                        },
                                        new String[]{"", imagePath, ImageName,edJobTitle.getText().toString().trim()});

                                image_layout.setVisibility(View.VISIBLE);
                                Show_Grid_Images(ImageName);
                            } else {
                                db.insertintoTable(getActivity(),
                                        db.TABLE_OFF_GALLERY, new String[]{
                                                db.TABLE_OFF_GALLERY_IMAGE_DESC,
                                                db.TABLE_OFF_GALLERY_IMAGE_PATH,
                                                db.TABLE_OFF_GALLERY_IMAGE_NAME,
                                                db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE
                                        },
                                        new String[]{edJobDesc.getText().toString().trim(), imagePath, ImageName,edJobTitle.getText().toString().trim()});

                                image_layout.setVisibility(View.VISIBLE);
                                Show_Off_Grid_Images(edJobTitle.getText().toString().trim());
                            }


                           /* Intent i =  new Intent(getActivity(),Image.class);
                            i.putExtra("Imagepath",imagePath);
                            i.putExtra("ImageName",ImageName);
                            i.putExtra("title",edJobTitle.getText().toString().trim());
                            startActivity(i);*/

                        }
                    }
                } else if(requestCode == TAKE_PICTURE) {

                    //Uri selectedImage = imageUri;
                    //Uri uri ;
                    try {
                        //System.out.println("image uri here :"+imageUri);
                        //uri = Uri.fromFile(new File(imageUri.toString().replace("content://","")));
                        //uri = Uri.fromFile(file);
                    } catch (Exception e) {
                        Log.e("Excep",e.getMessage());
                        //uri = data.getData();
                    }
                    ContentResolver cr = getActivity().getContentResolver();
                    //Bitmap selectedImageBitMap = MediaStore.Images.Media.getBitmap(this.getActivity().getContentResolver(), uri);
                    System.gc();
                    //Bitmap immagex = selectedImageBitMap;
                    //getActivity().getContentResolver().notifyChange(selectedImage, null);
                    try {
                        imagePath = imageUri.toString().replace("content://picservice.enaviya.com.picservice.provider/mediaimages/",Environment.getExternalStorageDirectory()+"/");
                        //imagePath = file.toString();
                        Log.i(TAG, "imagePath: " + imagePath);
                        File f = new File(imagePath);
                        moveFile(f.getParent(),f.getName());
                        if(NetworkConnection.isNetworkConnected(getActivity())){
                            db.insertintoTable(getActivity(),
                                    db.TABLE_GALLERY, new String[]{
                                            db.TABLE_GALLERY_IMAGE_DESC,
                                            db.TABLE_GALLERY_IMAGE_PATH,
                                            db.TABLE_GALLERY_IMAGE_NAME,
                                            db.TABLE_GALLERY_IMAGE_JOBTITLE
                                    },
                                    new String[]{"", imagePath, ImageName,edJobTitle.getText().toString().trim()});
                            //Bitmap bm = BitmapFactory.decodeFile(imageUri.toString().replace("file://",""));
                            image_layout.setVisibility(View.VISIBLE);
                            Show_Grid_Images(ImageName);
                        } else {
                            db.insertintoTable(getActivity(),
                                    db.TABLE_OFF_GALLERY, new String[]{
                                            db.TABLE_OFF_GALLERY_IMAGE_DESC,
                                            db.TABLE_OFF_GALLERY_IMAGE_PATH,
                                            db.TABLE_OFF_GALLERY_IMAGE_NAME,
                                            db.TABLE_OFF_GALLERY_IMAGE_JOBTITLE
                                    },
                                    new String[]{edJobDesc.getText().toString().trim(), imagePath, ImageName,edJobTitle.getText().toString().trim()});

                            image_layout.setVisibility(View.VISIBLE);
                            Show_Off_Grid_Images(edJobTitle.getText().toString().trim());
                        }

                        /*Intent i =  new Intent(getActivity(),Image.class);
                        i.putExtra("Imagepath",imagePath);
                        i.putExtra("ImageName",ImageName);
                        i.putExtra("title",edJobTitle.getText().toString().trim());
                        startActivity(i);*/

                    } catch (Exception e) {
                        Utilities.logError(getActivity(), TAG, "onCameraView", e.getMessage(), "Failed to load");
                        Log.e("Camera", e.toString());
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Utilities.showMessageAlertDialog(getActivity(), "Image selection has been cancelled");
            }
        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "onActivityResult", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }


    private void moveFile(String inputPath, String inputFile) {

        System.out.println("inputPath :"+inputPath);
        System.out.println("inputFile :"+inputFile);

        InputStream in = null;
        OutputStream out = null;
        try {
            //create output directory if it doesn't exist
            File dir = new File (Environment.getExternalStorageDirectory(), "/PicService");
            if (!dir.exists())
            {
                dir.mkdirs();
            }

            in = new FileInputStream(inputPath + "/"+inputFile);
            out = new FileOutputStream(dir.getPath() +"/"+inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();

            // write the output file
            out.flush();
            out.close();

            // delete the original file
            //new File(inputPath + inputFile).delete();

            File f = new File(dir.getPath() +"/"+inputFile);
            String extension = "jpg";
            String filename = "";

            if(f.getName().contains(".")){
                String[] parts = f.getName().split("\\.");
                filename = parts[0];
                extension = parts[1];
            }
            //System.out.println("parent file : "+f.getParentFile()+":: exten:"+extension);
            if(extension.equalsIgnoreCase("")){
                File newFile = new File(f.getParentFile()+"/"+filename+"_"+String.valueOf(System.currentTimeMillis())+"."+extension);
                f.renameTo(newFile);
                imagePath = newFile.getAbsolutePath();
                System.out.println("absolute path"+newFile.getAbsolutePath());
            } else {
                File newFile = new File(f.getParentFile()+"/"+filename+"_"+String.valueOf(System.currentTimeMillis())+"."+extension);
                f.renameTo(newFile);
                imagePath = newFile.getAbsolutePath();
                System.out.println("absolute path"+newFile.getAbsolutePath());
            }
        }

        catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    public static String getPath(Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;

    }


    @Override
    public void onDestroy() {

        super.onDestroy();


    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public class ProjectAuthHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            RequestBody body = RequestBody.create(JSON, params[1]);
            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.post(body);
            builder.addHeader("Authorization",Session.getAuth());
            Request request = builder.build();

            System.out.println("URl :" +params[0]);
            System.out.println("body :" +params[1]);

            try {
                okhttp3.Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            System.out.println("project details :: "+res);
            Id_ImagePath.clear();
            try {
                JSONObject jObj = new JSONObject(res);
                String Proj_Title = jObj.getString("projectTitle");
                if(Proj_Title.equalsIgnoreCase(edJobTitle.getText().toString().trim())){
                    db.insertintoTable(getActivity(),
                            db.TABLE_JOB, new String[]{
                                    db.TABLE_JOB_TITLE,
                                    db.TABLE_JOB_DESC,
                                    db.TABLE_JOB_IMAGE_NAME
                            },
                            new String[]{edJobTitle.getText().toString().trim(),
                                    edJobDesc.getText().toString().trim(),ImageName});


                    List<String[]> ImagePath =  new ArrayList();
                    ImagePath = db.getQueryData(getActivity(),
                            "select * from " + db.TABLE_GALLERY + " where "+db.TABLE_GALLERY_IMAGE_JOBTITLE + " = '"+edJobTitle.getText().toString().trim() +"'");

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd,00:00");
                    String formattedDate = df.format(c.getTime());

                    req_api ra = new req_api();
                    req_api.IdProject rId = new req_api().new IdProject();

                    ra.setId(0);
                    ra.setDescription(edJobDesc.getText().toString().trim());
                    ra.setContenttype("PNG");
                    ra.setCreatedDate(formattedDate);
                    rId.setProjectTitle(edJobTitle.getText().toString().trim());
                    ra.setIdProject(rId);
                    String values = new Gson().toJson(ra);

                    System.out.println("Image PAth size :: "+ImagePath.size());
                    for(int i = 0; i < ImagePath.size(); i++){
                        /*File photo = new File(ImagePath.get(i)[2]);
                        File newFile = new File(String.valueOf(System.currentTimeMillis()));
                        photo.renameTo(newFile);*/
                        Id_ImagePath.add(ImagePath.get(i)[2]);
                        System.out.println("Image PAth :: "+ImagePath.get(i)[2]);
                    }
                    CallHandler(values,Id_ImagePath);

                    // check once


                } else {
                    Spinnerdialog.dismiss();
                    Utilities.showMessageAlertDialog(getActivity(),"Project title must be unique");
                }

            } catch (Exception e){
                if(e.getMessage().equalsIgnoreCase("No value for projectTitle")){
                    Spinnerdialog.dismiss();
                    Utilities.showMessageAlertDialog(getActivity(),"Project title must be unique");
                } else{
                    Spinnerdialog.dismiss();
                    Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
                }
                Log.e("on failed adding:",e.getMessage());
            }

        }
    }

    private void CallHandler(String values, List<String> Imagepath) {
        try{
            idSize = Imagepath.size();
            if(idSize == 0){Spinnerdialog.dismiss();}
           // for(int i=0;i<Imagepath.size();i++){

                //File f = new File(Imagepath.get(i));

                System.out.println("file here :" +Session.getAuth() );

                MultipartRequest mreq = new MultipartRequest(BaseBuilder.Documents,new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getActivity(), "Upload failed!\r\n" + error.toString(), Toast.LENGTH_SHORT).show();
                        Spinnerdialog.dismiss();
                        Utilities.showMessageAlertDialog(getActivity(), error.toString());
                        Log.i("Error :"," error.toString "+error.toString());
                    }},new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(getActivity(), "Upload successfully!", Toast.LENGTH_SHORT).show();
                            Log.i("Rsponse "," resultResponse "+response);
                            //String resultResponse = new String(response.data);
                            //Log.i("Rsponse "," resultResponse "+resultResponse);
                            spinning = spinning+1;
                            //if(idSize==spinning){
                            dialog.dismiss();// check once
                            Spinnerdialog.dismiss();
                            if(update.equalsIgnoreCase("true")){
                                Utilities.showMessageAlertDialog(getActivity(), "Project updated sucessfully");
                            }
                            else
                            {
                                Utilities.showMessageAlertDialog(getActivity(), "Project added sucessfully");

                            }
                            db.deleteTable(getActivity(),db.TABLE_GALLERY);
                            DeleteDIR();
                            GetJobDetails();
                            // }

                        } catch (Exception e){
                            Log.i("Error1 :"," error.toString "+e.getMessage());
                        }
                    }

                },Imagepath,values,Session.getAuth(),getActivity());

                mreq.setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 120000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 120000;
                    }

                    @Override
                    public void retry(VolleyError error) throws VolleyError {
                        Log.i("Error :"," error.toString "+error.toString());
                    }
                });

                VolleySingleton.getInstance(getActivity()).addToRequestQueue(mreq);
            //}
        }
        catch (Exception e){
            Spinnerdialog.dismiss();
            Log.e(" CallHandler ",e.getMessage());
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        try {
            jobDetails_Adapter.getFilter().filter(newText);
        } catch (Exception e){

        }
        return false;
    }


    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }


    private void check_dialog() {
        try {
            ProgressBar pb;
            //Spinnerdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Spinnerdialog.setContentView(R.layout.progressbar);
            Spinnerdialog.setCancelable(false);
            Spinnerdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pb = (ProgressBar) Spinnerdialog.findViewById(R.id.progressBar);
            pb.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffb600"), PorterDuff.Mode.MULTIPLY);
            Spinnerdialog.show();
        } catch (Exception e){
            Log.e("check_dialog()" , e.getMessage());
            Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
        }
    }




}


