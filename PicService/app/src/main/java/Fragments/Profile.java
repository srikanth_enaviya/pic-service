package Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.SessionManager;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 09-01-2018.
 */
public class Profile  extends Fragment {
    private static final String TAG = Profile.class.getSimpleName();
    private SessionManager Session;
    DatabaseHandler db;
    String information = "";
    TextView mailID_tv,mail_tv,fname_tv,sname_tv,gender_tv;

    public Profile() {
        // Required empty public constructor
    }

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState){
        View fragment = null;
        try {
            fragment = inflater.inflate(R.layout.fragment_profile, container, false);
            Bundle bundle = this.getArguments();
            information = bundle.getString("Information");
            Session = new SessionManager(getActivity());
            db =  new DatabaseHandler(getActivity());
            mailID_tv = (TextView) fragment.findViewById(R.id.mailID_tv);
            mail_tv = (TextView) fragment.findViewById(R.id.mail_tv);
            fname_tv = (TextView) fragment.findViewById(R.id.fname_tv);
            sname_tv  = (TextView) fragment.findViewById(R.id.sname_tv);
            gender_tv = (TextView) fragment.findViewById(R.id.gender_tv);

            SetValues();




        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "onCreateView", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
        return fragment;
    }

    private void SetValues() {
        try{
            mailID_tv.setText(Session.getUsername());
            mail_tv.setText(Session.getUsername());
            fname_tv.setText(Session.getFName());
            sname_tv.setText(Session.getSName());
            gender_tv.setText(Session.getGender());
        }
        catch (Exception e){
            Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
        }
    }
}
