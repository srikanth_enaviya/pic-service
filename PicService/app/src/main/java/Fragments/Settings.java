package Fragments;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.SessionManager;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 09-01-2018.
 */
public class Settings extends Fragment {
    private static final String TAG = Settings.class.getSimpleName();
    private SessionManager Session;
    DatabaseHandler db;
    String information = "";
    TextView version_tv,tandc;

    public Settings() {
        // Required empty public constructor
    }

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState){
        View fragment = null;
        try {
            fragment = inflater.inflate(R.layout.fragment_settings, container, false);
            Bundle bundle = this.getArguments();
            information = bundle.getString("Information");
            Session = new SessionManager(getActivity());
            db =  new DatabaseHandler(getActivity());
            version_tv = (TextView) fragment.findViewById(R.id.version_tv);
            tandc = (TextView) fragment.findViewById(R.id.tandc);


            SetValues();



        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "onCreateView", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
        return fragment;
    }

    private void SetValues() {
        try {
            SpannableString content = new SpannableString("Terms and Conditions");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            tandc.setText(content);

            PackageManager packageManager = getActivity().getPackageManager();
            if(packageManager != null){
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(getActivity().getPackageName(), 0);
                    version_tv.setText(""+packageInfo.versionName);
                } catch (PackageManager.NameNotFoundException e) {

                }
            }
        }
        catch (Exception e){
            Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
        }
    }

}
