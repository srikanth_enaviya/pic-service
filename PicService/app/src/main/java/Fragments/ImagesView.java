package Fragments;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Adapters.Image_gv_Adapter;
import api.BaseBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import picservice.enaviya.com.picservice.DatabaseHandler;
import picservice.enaviya.com.picservice.NetworkConnection;
import picservice.enaviya.com.picservice.R;
import picservice.enaviya.com.picservice.SessionManager;
import picservice.enaviya.com.picservice.Utilities;

/**
 * Created by user on 18-09-2017.
 */
public class ImagesView extends Fragment {
    private static final String TAG = ImagesView.class.getSimpleName();
    private SessionManager Session;
    DatabaseHandler db;
    GridView image_gv;
    String information = "";
    public static List<String> Image_Uri = new ArrayList();
    Dialog Spinnerdialog;
    ImageView image_view;
    Bitmap bmp;
    List<String> Id_ImageList = new ArrayList();
    List<String> Id_ImageName = new ArrayList();
    int idSize = 0,spinning =0;
    TextView image_size_tv;

    public ImagesView() {
        // Required empty public constructor
    }

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState){
        View fragment = null;
        try {
            fragment = inflater.inflate(R.layout.fragment_imageview, container, false);
            Spinnerdialog = new Dialog(getActivity());
            Bundle bundle = this.getArguments();
            information = bundle.getString("Information");
            Session = new SessionManager(getActivity());
            db =  new DatabaseHandler(getActivity());
            image_gv =(GridView) fragment.findViewById(R.id.image_gv);
            image_view = (ImageView) fragment.findViewById(R.id.image_view);
            ShowGridview(information);

        } catch (Exception e) {
            Utilities.logError(getActivity(), TAG, "onCreateView", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
        return fragment;
    }

    private void ShowGridview(String information) {
        try {

            check_dialog();

            if(NetworkConnection.isNetworkConnected(getActivity())){
                ImageIDHandler imageidHandler = new ImageIDHandler();
                imageidHandler.execute(BaseBuilder.Image_ID_Details+information);
            } else {
                Spinnerdialog.dismiss();
                Utilities.showMessageAlertDialog(getActivity(), getResources().getString(R.string.InternetConnection));
            }
        }
        catch (Exception e){
            Log.e("Exception Images", e.getMessage());
        }
    }

    public class ImageIDHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.addHeader("Authorization",Session.getAuth());
            Request request = builder.build();

            System.out.println("URl :" +params[0]);


            try {
                okhttp3.Response response = client.newCall(request).execute();
                return response.body().string();
            }
            catch (Exception e){
                Spinnerdialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            //System.out.println("Image ID "+res);
            try {
                JSONArray arr = new JSONArray(res);
                Id_ImageList.clear(); Id_ImageName.clear(); Image_Uri.clear();
                for (int i = 0; i < arr.length();i++){
                    JSONObject ID_details = arr.getJSONObject(i);
                    //System.out.println("Image ID: "+ID_details.getString("id"));
                    Id_ImageList.add(ID_details.getString("id"));
                    Id_ImageName.add(ID_details.getString("filename"));
                    //System.out.println(ID_details.getString("id"));

                }
                CallImageAPI(Id_ImageList,Id_ImageName);
                //Spinnerdialog.dismiss();

            } catch (Exception e) {
                Spinnerdialog.dismiss();
                Log.e("JobAuthHandler" , e.getMessage());
                Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
            }

        }
    }

    private void CallImageAPI(List<String> id, List<String> filename) {
        try {
            File dir = new File (Environment.getExternalStorageDirectory(), "/PicService");
            if (!dir.exists())
            {
                dir.mkdirs();
            }
            idSize = id.size();
            //image_size_tv.setText("Please wait Images Loading 0/"+id.size());
            Image_Uri.clear();
            for(int i =0; i< id.size();i++){
                //System.out.println("get id:"+id.get(i));
                Image_Uri.add(BaseBuilder.Image_Details_browser+Session.getAuth().replace("Bearer ","").trim()+"/"+id.get(i));
                //new Images().execute(BaseBuilder.Image_Details_browser+Session.getAuth().replace("Bearer","").trim()+"/"+id.get(i),id.get(i));
                //new Images().execute(id.get(i),filename.get(i));
            }
            //System.out.println("Size::"+Image_Uri.size());
            Image_gv_Adapter adapter = new Image_gv_Adapter(getActivity(), Image_Uri);

            image_gv.setAdapter(adapter);
            Spinnerdialog.dismiss();

        }
        catch (Exception e){
            Spinnerdialog.dismiss();
        }
    }


    /*class Images extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL wallpaperURL = new URL(params[0]);

                System.out.println(params[0]);

                URLConnection connection = wallpaperURL.openConnection();

                InputStream inputStream = new BufferedInputStream(wallpaperURL.openStream(), 10240);

                File cacheDir = getCacheFolder(getActivity());

                File cacheFile = new File(cacheDir, params[1]+".jpg");

                FileOutputStream outputStream = new FileOutputStream(cacheFile);



                byte buffer[] = new byte[1024];

                int dataSize;

                int loadedSize = 0;

                while ((dataSize = inputStream.read(buffer)) != -1) {

                    loadedSize += dataSize;

                    publishProgress(String.valueOf(loadedSize));

                    outputStream.write(buffer, 0, dataSize);
                }

                outputStream.close();

                String JsonResponse = "";


                try {
                    return JsonResponse;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Toast.makeText(FirstActivity.this, "hai:"+s, Toast.LENGTH_SHORT).show();
            System.out.println("Sucess");
            spinning = spinning+1;
            image_size_tv.setText("Please wait Images Loading "+spinning+"/"+idSize);
            if(idSize==spinning){
                Image_Uri.clear();
                File[] files = new File(Environment.getExternalStorageDirectory(), "/PicService").listFiles();
                for(File file : files){
                    if(file.isFile()){
                        Image_Uri.add(file.getAbsolutePath());
                        System.out.println("inside :"+file.getAbsolutePath());
                    }
                }
                Image_gv_Adapter adapter = new Image_gv_Adapter(getActivity(), Image_Uri);

                image_gv.setAdapter(adapter);
                Spinnerdialog.dismiss();

            }
        }

    }*/


    public File getCacheFolder(Context context) {

        File cacheDir = null;

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            cacheDir = new File("/storage/emulated/0/PicService");

            if (!cacheDir.isDirectory()) {

                cacheDir.mkdirs();

            }

        }

        if(!cacheDir.isDirectory()) {
            cacheDir = context.getCacheDir(); //get system cache folder
        }

        return cacheDir;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }



    private void check_dialog() {
        try {
            ProgressBar pb;
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Spinnerdialog.setContentView(R.layout.progressbar);
            Spinnerdialog.setCancelable(false);
            Spinnerdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pb = (ProgressBar) Spinnerdialog.findViewById(R.id.progressBar);
            image_size_tv = (TextView) Spinnerdialog.findViewById(R.id.image_size_tv);
            pb.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffb600"), PorterDuff.Mode.MULTIPLY);
            Spinnerdialog.show();
        } catch (Exception e){
            Log.e("check_dialog()" , e.getMessage());
            Utilities.showMessageAlertDialog(getActivity(),getResources().getString(R.string.something_went_wrong));
        }
    }
}



