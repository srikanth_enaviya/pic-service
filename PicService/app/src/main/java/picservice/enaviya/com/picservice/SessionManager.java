package picservice.enaviya.com.picservice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Created by user on 18-09-2017.
 */
public class SessionManager {
    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    public static final String PREF_NAME ="Pic Service";
    public static final String IS_LOGIN ="IsLoggedIn";
    public static final String Auth ="Auth";
    public static final String Refresh ="Refresh";
    public static final String ID ="ID";
    public static final String EmailId ="EmailId";
    public static final String USER ="USER";
    public static final String PASSWORD ="PASSWORD";
    public static final String FName ="FName";
    public static final String SName ="SName";
    public static final String Gender ="Gender";
    public static final String Yes ="Yes";


    public SessionManager(Context context)
    {
        this._context =context;
        pref = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public void checkLogin(){
        if(isLoggedIn()){
            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(i.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
        else
        {   Intent i=new Intent(_context,Register_Activity.class);
            i.addFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(i.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

        }
    }

    private boolean isLoggedIn()
    {

        return pref.getBoolean(IS_LOGIN,false);
    }


    public void LogoutUser()
    {
        editor.clear();
        editor.commit();

        Intent i=new Intent(_context,LoginActivity.class);
        i.addFlags(i.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(i.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public void LoginDetails(String id,String uName, String EID,String Pwd) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(ID, id);
        editor.putString(USER, uName);
        editor.putString(EmailId, EID);
        editor.putString(PASSWORD,Pwd);
        editor.commit();
    }

    public void Auth(String auth,String refresh) {
        editor.putString(Auth, auth);
        editor.putString(Refresh, refresh);
        editor.commit();
    }

    public String getAuth() {
        // TODO Auto-generated method stub
        return pref.getString(Auth,"");
    }


    public String getRefresh() {
        // TODO Auto-generated method stub
        return pref.getString(Refresh,"");
    }


    public String getUsername() {
        // TODO Auto-generated method stub
        return pref.getString(USER,"");
    }



    public void newUser(String yes) {
        editor.putString(Yes, yes);
        editor.commit();
    }

    public String getnewUser() {
        // TODO Auto-generated method stub
        return pref.getString(Yes,"");
    }



    public void ProfileDetails(String fName, String sName,String gender) {
        editor.putString(FName, fName);
        editor.putString(SName, sName);
        editor.putString(Gender, gender);
        editor.commit();
    }


    public String getFName() {
        // TODO Auto-generated method stub
        return pref.getString(FName,"");
    }

    public String getSName() {
        // TODO Auto-generated method stub
        return pref.getString(SName,"");
    }

    public String getGender() {
        // TODO Auto-generated method stub
        return pref.getString(Gender,"");
    }
}

