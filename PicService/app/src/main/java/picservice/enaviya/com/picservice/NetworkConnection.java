package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

import backend_service.Offline_Images;

/**
 * Created by user on 09-11-2017.
 */
public class NetworkConnection {

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isNetworkConnected1(Offline_Images activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
