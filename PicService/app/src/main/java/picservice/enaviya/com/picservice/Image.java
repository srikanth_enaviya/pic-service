package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;

import Fragments.Home;

public class Image extends Activity {

    String imagepath = "",title ="",ImageName ="";

    ImageView iv,enter;
    EditText caption;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_image);

            iv = (ImageView) findViewById(R.id.iv);
            enter = (ImageView) findViewById(R.id.enter);

            caption = (EditText) findViewById(R.id.caption_tv);

            Intent i = getIntent();

            imagepath = i.getStringExtra("Imagepath");
            title = i.getStringExtra("title");
            ImageName = i.getStringExtra("ImageName");

            File imgFile = new File(imagepath);

            if(imgFile.exists()){

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                iv.setImageBitmap(myBitmap);
            }

                enter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(caption.getText().toString().trim().equalsIgnoreCase("")){
                        Utilities.showMessageAlertDialog(Image.this,"Add a caption");
                    } else {
                        DatabaseHandler.updateROW(Image.this,
                                DatabaseHandler.TABLE_GALLERY,
                                new String[] { DatabaseHandler.TABLE_GALLERY_IMAGE_DESC },
                                new String[] { caption.getText().toString().trim()},
                                new String[] { DatabaseHandler.TABLE_GALLERY_IMAGE_JOBTITLE },
                                new String[] { title });
                        System.out.println("updated");
                        finish();
                    }

                }
            });

           // System.out.println("image" + imagepath +":: title :"+title);

        } catch (Exception e){
            Utilities.showMessageAlertDialog(Image.this,getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void onBackPressed() {
        DatabaseHandler.deleteRows(Image.this, DatabaseHandler.TABLE_GALLERY,
                DatabaseHandler.TABLE_GALLERY_IMAGE_PATH, imagepath);
        Home.Show_Grid_Images(ImageName);
        finish();
    }
}
