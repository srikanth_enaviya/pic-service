package picservice.enaviya.com.picservice;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.util.CharsetUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultipartRequest<T> extends Request<JSONObject> {

    private MultipartEntityBuilder entity = MultipartEntityBuilder.create();

    private final Response.Listener<JSONObject> mListener;
    private HttpEntity httpentity;

    private String mToken;
    private final List<String> mImageFile;

    SessionManager Session;



    public MultipartRequest(String url, Response.ErrorListener errorListener, Response.Listener<JSONObject> listener, final List<String> file, String values, String Auth, Context context)
    {
        super(Method.POST, url, errorListener);
        mListener = listener;
        mToken = Auth;
        mImageFile = file;
      /*  for(int i = 0; i < mImageFile.size();i++) {
            File F = new File(mImageFile.get(i));
            System.out.println("MultipartRequest files : "+F);
        }*/


        Session = new SessionManager(context);

        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        try {
            entity.setCharset(CharsetUtils.get("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        buildMultipartEntity(file,values);

    }




    private void buildMultipartEntity(List<String> mFile, String data)
    {
        // Log.i("object",""+mJsonObject.toString());
        try
        {
            for(int i = 0; i < mFile.size();i++){
                File F= new File (mFile.get(i));
                entity.addPart("files[]", new FileBody(F));
            }

            //entity.addBinaryBody("file",mFile);


            entity.addTextBody("document",data);


            httpentity = entity.build();

            entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            entity.setLaxMode().setBoundary("UTF-8");//.setCharset(Charset.forName("UTF-8"));

            //System.out.println(httpentity.getContentType().getValue());

            /*ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try
            {
                httpentity.writeTo(bos);
            }
            catch (IOException e)
            {
                VolleyLog.e("IOException writing to ByteArrayOutputStream");
            }

            System.out.println(bos.toByteArray());*/

        }
        catch (Exception e)
        {
            Log.e("",e.getMessage());
            //VolleyLog.e("UnsupportedEncodingException");
        }
    }



    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        for(int i = 0; i < mImageFile.size();i++) {
            File F = new File(mImageFile.get(i));
            System.out.println("MD5Checksum file :: "+F);
            String localFileHash = MD5Checksum.getMd5Hash(F);
            headers.put("Content-MD5",localFileHash);
        }

        headers.put("Authorization", mToken);

        return headers;

/*        ArrayMap<String, String> mHeaders =  new ArrayMap<String, String>();
        System.out.println("header here :" +Session.getAuth());
        mHeaders.put("Authorization", Session.getAuth()) ;
        return (mHeaders != null) ? mHeaders : super.getHeaders();*/
    }
    @Override
    public String getBodyContentType()
    {
        try {
            String contentTypeHeader = entity.build().getContentType().getValue();
            System.out.println("contentTypeHeader : "+entity.build().getContentType().getValue());
        } catch (Exception e){
            Log.e("", e.getMessage());
        }
        return entity.build().getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try
        {
            //httpentity.writeTo(bos);
            entity.build().writeTo(bos);
            System.out.println("Perfect");
        }
        catch (IOException e)
        {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

        String uploadResult = null;
        JSONObject json;
        try {
            uploadResult = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));

            json = new JSONObject();
            json.put("UploadResult",uploadResult);

            return Response.success(json, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch  (JSONException e) {
            return Response.error(new ParseError(e));
        }

    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {

        // Parser for the network error

        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {

            // In case of http error
            return volleyError;

        } else {

            // In case of: TimeoutError, NoConnectionError ....
            String errorMessage = new String(volleyError.toString());

            // TODO: Change the error checking method
            if (errorMessage.contains("TimeoutError")) {
                errorMessage = "The server is unreachable.\nPlease try again later";
            } else if (errorMessage.contains("NoConnectionError")) {
                errorMessage = "Please check your internet connectivity and then try again";
            }

            // Other errors
            return new VolleyError(errorMessage);
        }
    }



    @Override
    protected void deliverResponse(JSONObject response)
    {
        mListener.onResponse(response);

    }


    @Override
    public Request<?> setTag(Object tag) {
        return super.setTag(tag);
    }


}