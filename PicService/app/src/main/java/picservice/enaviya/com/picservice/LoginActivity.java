package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import api.BaseBuilder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class LoginActivity extends Activity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    TextView forgot_pwd;
    Button register;

    EditText edLoginID, edPassword;

    Button LoginBtn;

    private static final float BLUR_RADIUS = 25f;

    SessionManager Session;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login_new);
            dialog =  new Dialog(LoginActivity.this);
            Session =  new SessionManager(LoginActivity.this);
            forgot_pwd = (TextView) findViewById(R.id.forgot_pwd);
            register = (Button) findViewById(R.id.register);

            edLoginID = (EditText) findViewById(R.id.edLoginID);
            edPassword = (EditText) findViewById(R.id.edPassword);

            LoginBtn =(Button) findViewById(R.id.LoginBtn);

            /*ImageView imageView = (ImageView) findViewById(R.id.bg_iv);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.bg_image);
            Bitmap blurredBitmap = blur(bitmap);
            imageView.setImageBitmap(blurredBitmap);*/

            forgot_pwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(LoginActivity.this, forgot_pwd_Activity.class);
                    startActivity(i);
                }
            });

            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(LoginActivity.this, Register_Activity.class);
                    startActivity(i);
                }
            });

            LoginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edLoginID.getText().toString().trim().equalsIgnoreCase("") || edPassword.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(LoginActivity.this, "Please fill all fileds", Toast.LENGTH_SHORT).show();

                    } else {
                        if(NetworkConnection.isNetworkConnected(LoginActivity.this)){
                            JSONObject Login = new JSONObject();
                            try {
                                Login.put("userName", edLoginID.getText().toString().trim());
                                Login.put("userPassword", edPassword.getText().toString().trim());
                                check_dialog();
                                LoginAuthHandler loginAuthHandler = new LoginAuthHandler();
                                loginAuthHandler.execute(BaseBuilder.Login, String.valueOf(Login));
                            } catch (Exception e){
                                dialog.dismiss();
                                Log.e(TAG,e.getMessage());
                                Toast.makeText(LoginActivity.this, "OOps Something wrong", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utilities.showMessageAlertDialog(LoginActivity.this, getResources().getString(R.string.InternetConnection));
                        }
                    }
                }
            });

        } catch (Exception e) {
            Toast.makeText(LoginActivity.this, "OOps Something wrong", Toast.LENGTH_SHORT).show();
        }
    }


    public class LoginAuthHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            RequestBody body = RequestBody.create(JSON, params[1]);
            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.post(body);
            Request request = builder.build();

            System.out.println("URl :" +params[0]);
            System.out.println("body :" +params[1]);

            try {
                okhttp3.Response response = client.newCall(request).execute();
                //String response_auth = response.headers().get("authorization")+"-"+response.headers().get("refresh")+"-"+response.body().toString();
                String response_auth = "Authorization:" + response.headers().get("authorization")+"Refresh:"+response.headers().get("refresh").toString();
                return response_auth;
            }catch (Exception e){
                dialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                super.onPostExecute(res);
                System.out.println("Login Response : "+res);
                String authToken = res.substring(res.indexOf(":")+1, res.indexOf("Refresh"));
                String refreshToken = res.substring(res.indexOf("Refresh:") + "Refresh:".length() + 1 );
                //String[] parts = res.split("-");
                if(res!=null){
                    if (authToken.equalsIgnoreCase(null) || authToken == null || authToken.equalsIgnoreCase("null")) {
                        dialog.dismiss();
                        Utilities.showMessageAlertDialog(LoginActivity.this, getResources().getString(R.string.something_went_wrong));
                    } else if (authToken.equalsIgnoreCase("")) {
                        dialog.dismiss();
                        Utilities.showMessageAlertDialog(LoginActivity.this, getResources().getString(R.string.something_went_wrong));
                    } else {
                        //Session.Auth(parts[0],parts[1]);
                        Session.Auth(authToken, refreshToken);

                        LoginHandler loginHandler = new LoginHandler();
                        loginHandler.execute(BaseBuilder.UsersList);
                    /*if(NetworkConnection.isNetworkConnected(LoginActivity.this)){
                        LoginHandler loginHandler = new LoginHandler();
                        loginHandler.execute(BaseBuilder.UsersList);
                    } else {
                        Utilities.showMessageAlertDialog(LoginActivity.this, getResources().getString(R.string.InternetConnection));
                    }*/

                    }
                } else {
                    Utilities.showMessageAlertDialog(LoginActivity.this, getResources().getString(R.string.something_went_wrong));
                }

            } catch (Exception e){
                dialog.dismiss();
                Log.e(TAG,e.getMessage());
                Utilities.showMessageAlertDialog(LoginActivity.this,getResources().getString(R.string.invalidCredentials));
            }
        }
    }


    public class LoginHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.addHeader("Authorization",Session.getAuth());
            Request request = builder.build();

            System.out.println(Session.getAuth()+"::URL 2 ::" +params[0]);

            try {
                okhttp3.Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                dialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            System.out.println("Login : "+res);
            try {
                Boolean s = false;
                JSONArray arr = new JSONArray(res);
                for (int i = 0; i < arr.length();i++){
                    JSONObject List_details = arr.getJSONObject(i);
                    if(edLoginID.getText().toString().trim().equalsIgnoreCase(List_details.getString("userName"))){
                        JSONObject details = new JSONObject(List_details.getString("userDetails"));
                        String FirstName = details.getString("firstName");
                        Session.LoginDetails(List_details.getString("idUser"),edLoginID.getText().toString().trim(),edLoginID.getText().toString().trim(),edPassword.getText().toString().trim());
                        Session.newUser("");
                        //System.out.println(FirstName+":"+details.getString("surname")+":"+details.getString("gender"));
                        Session.ProfileDetails(FirstName,details.getString("surname"),details.getString("gender"));
                        s = true;
                    }
                }
                if(s){
                    dialog.dismiss();
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                } else {
                    dialog.dismiss();
                    Utilities.showMessageAlertDialog(LoginActivity.this,getResources().getString(R.string.invalidCredentials));
                }
            } catch (Exception e){
                dialog.dismiss();
                Utilities.showMessageAlertDialog(LoginActivity.this,getResources().getString(R.string.something_went_wrong));
                Log.e(TAG,e.getMessage());
            }

        }
    }

    public Bitmap blur(Bitmap image) {
        Bitmap outputBitmap = null;
        try {
            if (null == image) return null;

            outputBitmap = Bitmap.createBitmap(image);
            final RenderScript renderScript = RenderScript.create(this);
            Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
            Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

            //Intrinsic Gausian blur filter
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

        } catch (Exception e) {
            Toast.makeText(LoginActivity.this,getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }
        return outputBitmap;
    }


    private void check_dialog() {
        try {
            ProgressBar pb;
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressbar);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pb = (ProgressBar) dialog.findViewById(R.id.progressBar);
            pb.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffb600"), PorterDuff.Mode.MULTIPLY);
            dialog.show();
        } catch (Exception e){
            Utilities.showMessageAlertDialog(LoginActivity.this,getResources().getString(R.string.something_went_wrong));
        }
    }
}
