package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import ApiRequest.Register;
import ApiResponse.RegisterResponse;
import api.BaseBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class Register_Activity extends Activity {

    Button male,female;

    Button Signin_tv;

    EditText edUserName,edEmail,edPassword,edCnfmPassword;

    private static final float BLUR_RADIUS = 25f;

    Button RegisterBtn;

    Call<RegisterResponse> call;
    Dialog dialog;
    String Gender = "F";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register_new);
            dialog =  new Dialog(Register_Activity.this);

           /* ImageView imageView = (ImageView) findViewById(R.id.bg_iv);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.bg_image);
            Bitmap blurredBitmap = blur(bitmap);
            imageView.setImageBitmap(blurredBitmap);*/


            RegisterBtn = (Button) findViewById(R.id.RegisterBtn);
            Signin_tv = (Button) findViewById(R.id.Signin_tv);
            male = (Button) findViewById(R.id.male);
            female = (Button) findViewById(R.id.female);

            edUserName = (EditText) findViewById(R.id.edUserName);
            edEmail = (EditText) findViewById(R.id.edEmail);
            edPassword = (EditText) findViewById(R.id.edPassword);
            edCnfmPassword = (EditText) findViewById(R.id.edCnfmPassword);

            male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Gender = "M";
                    male.setBackgroundColor(getResources().getColor(R.color.appThm));
                    female.setBackgroundColor(getResources().getColor(R.color.highlight));
                }
            });

            female.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Gender = "F";
                    female.setBackgroundColor(getResources().getColor(R.color.appThm));
                    male.setBackgroundColor(getResources().getColor(R.color.highlight));
                }
            });

            Signin_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Register_Activity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            });

            RegisterBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edUserName.getText().toString().trim().equalsIgnoreCase("") || edEmail.getText().toString().trim().equalsIgnoreCase("") || edPassword.getText().toString().trim().equalsIgnoreCase("") || edCnfmPassword.getText().toString().trim().equalsIgnoreCase("")) {

                        Toast.makeText(Register_Activity.this, "Please fill all the details !", Toast.LENGTH_SHORT).show();

                    } else if (!edEmail.getText().toString().trim().matches(emailPattern)) {

                        Toast.makeText(Register_Activity.this, "Please enter valid email id", Toast.LENGTH_SHORT).show();

                    } else if (!edPassword.getText().toString().trim().equalsIgnoreCase(edCnfmPassword.getText().toString().trim())) {

                        Toast.makeText(Register_Activity.this, "Confirm Password Doesn't match", Toast.LENGTH_SHORT).show();

                    } else {
                        if(NetworkConnection.isNetworkConnected(Register_Activity.this)){
                            check_dialog();
                            CALL_SIGIN_API();
                        } else {
                            Utilities.showMessageAlertDialog(Register_Activity.this, getResources().getString(R.string.InternetConnection));
                        }

                    }
                }
            });
        } catch (Exception e){
            Toast.makeText(Register_Activity.this, "Oops Something went wrong!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void CALL_SIGIN_API() {

        try {
            Register register = new Register();
            Register.UserDetails Userdetails =  new Register().new UserDetails();
            Register.Roles roles = new Register().new Roles();

            BaseBuilder baseBuilder = new BaseBuilder();
            Retrofit retrofit = baseBuilder.getRetrofitInstance(null, null, Register_Activity.this);

            register.setUserName(edEmail.getText().toString().trim());
            register.setUserPassword(edPassword.getText().toString().trim());
            register.setEnabled("true");

            roles.setUserType("DEVELOPER");
            register.setRoles(roles);

            Userdetails.setFirstName(edUserName.getText().toString().trim());
            Userdetails.setSurname(edUserName.getText().toString().trim());
            Userdetails.setGender(Gender);
            register.setUserDetails(Userdetails);

            Log.i("Request register  :",new Gson().toJson(register) );

            RegisterUser registerUser = retrofit.create(RegisterUser.class);
            call = registerUser.registerUser(register);
            call.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                    if (!call.isCanceled())
                        onRegisterSuccess(response.body());
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    if (!call.isCanceled()) {
                        dialog.dismiss();
                        Log.e("on Failure", t.getMessage());
                        Toast.makeText(Register_Activity.this, "Login Failed. Please try again after some time.", Toast.LENGTH_SHORT).show();
                    }
                }
            });


        } catch (Exception e){
            dialog.dismiss();
            Toast.makeText(Register_Activity.this, "Oops Something went wrong!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void onRegisterSuccess(RegisterResponse body) {

        try {

            Log.i("onRegisterSuccess", "RegisterResponse: " + new Gson().toJson(body));

            if (body != null) {
                if (body.getResponseCode().equalsIgnoreCase("200")) {
                    LayoutInflater li = LayoutInflater.from(Register_Activity.this);
                    View promptsView = li.inflate(R.layout.prompt_message, null);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Register_Activity.this);
                    alertDialogBuilder.setView(promptsView);
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    TextView lblMessage = (TextView) promptsView.findViewById(R.id.lblMessage);
                    lblMessage.setText(body.getResponseMessage());
                    Button btnOK = (Button) promptsView.findViewById(R.id.btnOK);
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i  = new Intent(Register_Activity.this,LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    alertDialog.show();
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    Toast.makeText(Register_Activity.this, "Register Failed. Please try again after some time.", Toast.LENGTH_SHORT).show();
                }
            } else {
                dialog.dismiss();
                Toast.makeText(Register_Activity.this, "Register Failed. Please try again after some time.", Toast.LENGTH_SHORT).show();
            }

        }
        catch (Exception e){
            dialog.dismiss();
            Log.e("onRegisterSuccess", e.getMessage());
            Toast.makeText(Register_Activity.this, "Oops Something went wrong!!", Toast.LENGTH_SHORT).show();

        }


    }

    public Bitmap blur(Bitmap image) {
        Bitmap outputBitmap = null;
        try {
            if (null == image) return null;

            outputBitmap = Bitmap.createBitmap(image);
            final RenderScript renderScript = RenderScript.create(this);
            Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
            Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

            //Intrinsic Gausian blur filter
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

        } catch (Exception e) {
            Toast.makeText(Register_Activity.this, "Oops Something wrong", Toast.LENGTH_SHORT).show();
        }
        return outputBitmap;
    }

    private void check_dialog() {
        ProgressBar pb;
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.progressbar);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pb = (ProgressBar) dialog.findViewById(R.id.progressBar);
        pb.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffb600"), PorterDuff.Mode.MULTIPLY);
        dialog.show();
    }


    public interface RegisterUser {
        @POST("appauth/user/")
        Call<RegisterResponse> registerUser(@Body Register register);
    }
}

