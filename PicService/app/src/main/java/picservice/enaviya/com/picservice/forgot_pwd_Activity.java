package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONObject;

import api.BaseBuilder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class forgot_pwd_Activity extends Activity {

    Button login_back,btnSubmit;
    EditText edUserId;

    private static final float BLUR_RADIUS = 25f;
    SessionManager Session;
    Dialog dialog;
    private static final String TAG = forgot_pwd_Activity.class.getSimpleName();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_forgot_pwd_new);

           /* ImageView imageView = (ImageView) findViewById(R.id.bg_iv);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.bg_image);
            Bitmap blurredBitmap = blur(bitmap);
            imageView.setImageBitmap(blurredBitmap);*/
            dialog =  new Dialog(forgot_pwd_Activity.this);
            Session =  new SessionManager(forgot_pwd_Activity.this);

            login_back = (Button) findViewById(R.id.login_tv);
            edUserId = (EditText) findViewById(R.id.edUserId);
            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            //help = (RelativeLayout) findViewById(R.id.help);

            login_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(forgot_pwd_Activity.this,LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            });

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(edUserId.getText().toString().trim().equalsIgnoreCase("")){
                        Utilities.showMessageAlertDialog(forgot_pwd_Activity.this,"Enter EmailId");
                    } else {
                        if(NetworkConnection.isNetworkConnected(forgot_pwd_Activity.this)) {
                            CALl_API();
                        } else {
                            Utilities.showMessageAlertDialog(forgot_pwd_Activity.this, getResources().getString(R.string.InternetConnection));
                        }
                    }
                }
            });

        } catch (Exception e) {
            Toast.makeText(forgot_pwd_Activity.this, "Oops Something wrong", Toast.LENGTH_SHORT).show();
        }
    }

    private void CALl_API() {
        try {
            JSONObject forgot = new JSONObject();
            try {
                forgot.put("userName", edUserId.getText().toString().trim());
                check_dialog();
                ForgotAuthHandler forgotAuthHandler = new ForgotAuthHandler();
                forgotAuthHandler.execute(BaseBuilder.Forgot, String.valueOf(forgot));
            } catch (Exception e){
                dialog.dismiss();
                Log.e(TAG,e.getMessage());
                Toast.makeText(forgot_pwd_Activity.this, "OOps Something wrong", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e){

        }
    }


    public class ForgotAuthHandler extends AsyncTask<String, String, String> {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected String doInBackground(String...params) {

            RequestBody body = RequestBody.create(JSON, params[1]);
            Request.Builder builder = new Request.Builder();
            builder.url(params[0]);
            builder.post(body);
            Request request = builder.build();

            System.out.println("URl :" +params[0]);
            System.out.println("body :" +params[1]);

            try {
                okhttp3.Response response = client.newCall(request).execute();
                return response.body().string();
            }catch (Exception e){
                dialog.dismiss();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                super.onPostExecute(res);
                System.out.println("Forgot Response : "+res);
                dialog.dismiss();



                if (res.equalsIgnoreCase(null)) {
                    dialog.dismiss();
                    Utilities.showMessageAlertDialog(forgot_pwd_Activity.this, getResources().getString(R.string.something_went_wrong));
                } else if (res.contains(edUserId.getText().toString().trim())) {
                    dialog.dismiss();
                    Utilities.showMessageAlertDialog(forgot_pwd_Activity.this, "Change Password link sent to your mailID : "+edUserId.getText().toString().trim());
                } else {
                    JSONObject details = new JSONObject(res);
                    String Status = details.getString("status");
                    if(Status.equalsIgnoreCase("500")){
                        Utilities.showMessageAlertDialog(forgot_pwd_Activity.this, getResources().getString(R.string.invalidCredentials));
                    } else {
                        Utilities.showMessageAlertDialog(forgot_pwd_Activity.this, getResources().getString(R.string.something_went_wrong));
                    }
                }
            } catch (Exception e){
                dialog.dismiss();
                Log.e(TAG,e.getMessage());
                Utilities.showMessageAlertDialog(forgot_pwd_Activity.this,getResources().getString(R.string.invalidCredentials));
            }
        }
    }


    public Bitmap blur(Bitmap image) {
        Bitmap outputBitmap = null;
        try {
            if (null == image) return null;

            outputBitmap = Bitmap.createBitmap(image);
            final RenderScript renderScript = RenderScript.create(this);
            Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
            Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

            //Intrinsic Gausian blur filter
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

        } catch (Exception e) {
            Toast.makeText(forgot_pwd_Activity.this, "OOps Something wrong", Toast.LENGTH_SHORT).show();
        }
        return outputBitmap;
    }

    private void check_dialog() {
        try {
            ProgressBar pb;
            //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progressbar);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pb = (ProgressBar) dialog.findViewById(R.id.progressBar);
            pb.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ffb600"), PorterDuff.Mode.MULTIPLY);
            dialog.show();
        } catch (Exception e){
            Utilities.showMessageAlertDialog(forgot_pwd_Activity.this,getResources().getString(R.string.something_went_wrong));
        }
    }
}

