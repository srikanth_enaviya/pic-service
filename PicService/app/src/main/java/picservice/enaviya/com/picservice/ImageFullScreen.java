package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ImageFullScreen extends Activity {

    private ImageLoader imageLoaderMain;
    private AnimateFirstDisplayListener animateFirstListener1;
    private DisplayImageOptions options;

    private static final String TAG = ImageFullScreen.class.getSimpleName();

    String Imagepath = "";
    ZoomableImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_image_full_screen);
            try {

                imageView = (ZoomableImageView) findViewById(R.id.imageview);
            } catch (Exception e) {
                Utilities.logError(ImageFullScreen.this, TAG, "onCreateView", e.getMessage(), getString(R.string.something_went_wrong));

            }


            imageLoaderMain = ImageLoader.getInstance();
            imageLoaderMain.init(ImageLoaderConfiguration.createDefault(ImageFullScreen.this));
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.mipmap.ic_launcher)
                    .showImageOnFail(R.mipmap.ic_launcher)
                    .resetViewBeforeLoading(true).cacheOnDisc(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .considerExifParams(true)
                    .displayer(new FadeInBitmapDisplayer(300)).build();

            Intent i = getIntent();
            Imagepath = i.getStringExtra("ImagePath");


            try {
                File imgFile = new File(Imagepath);


                if (imgFile.exists()) {
                    try {
                        System.out.println("ImageFullScreen : "+imgFile.getAbsolutePath());
                        imageLoaderMain.displayImage("file://" + imgFile.getAbsolutePath(), imageView, options, animateFirstListener1);
             /*   Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageView.setImageBitmap(myBitmap);*/
                    } catch (Exception e) {
                        Utilities.logError(ImageFullScreen.this, TAG, "onImageView", e.getMessage(), getString(R.string.something_went_wrong));
                    }

                } else {
                    Utilities.showMessageAlertDialog(ImageFullScreen.this, "Image Not Found");
                }
            } catch (Exception e) {
                Utilities.logError(ImageFullScreen.this, TAG, "onImageView", e.getMessage(), getString(R.string.something_went_wrong));
                e.printStackTrace();
            }
        } catch (Exception e){
            Utilities.showMessageAlertDialog(ImageFullScreen.this,getResources().getString(R.string.something_went_wrong));
        }

    }


    private static class AnimateFirstDisplayListener extends
            SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view,
                                      Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 0);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}

