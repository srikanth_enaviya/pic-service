package picservice.enaviya.com.picservice;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import Model.UserProfileDataModel;

/**
 * Created by user on 18-09-2017.
 */
public class Utilities {
    private static final String TAG = Utilities.class.getSimpleName();

    public static void logError(Context context, String module, String function, String errorMessage, String displayMessage) {
        Log.e(module, function + " : " + errorMessage);
        showMessageAlertDialog(context, displayMessage);
    }

    public static void showMessageAlertDialog(Context context, String message) {
        try {
            LayoutInflater li = LayoutInflater.from(context);
            View promptsView = li.inflate(R.layout.prompt_message, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setView(promptsView);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            TextView lblMessage = (TextView) promptsView.findViewById(R.id.lblMessage);
            lblMessage.setText(message);
            Button btnOK = (Button) promptsView.findViewById(R.id.btnOK);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.cancel();
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "error message: " + e.getMessage());
        }
    }

    public static void setProfilePicture(Context context, ImageView view) {
        try {
            if (view != null) {
                SessionManager  Session = new SessionManager(context);
                DatabaseHandler db = new DatabaseHandler(context);
                UserProfileDataModel userProfileDataModel = db.getProfileImage();
                File file = new File(userProfileDataModel.getImagePath());
                String imageName = userProfileDataModel.getImageName();

                String userName = Session.getUsername() ;

                if (userName.equalsIgnoreCase(imageName)) {
                    Bitmap bitmap;
                    if (file.exists()) {
                        bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        view.setImageBitmap(bitmap);
                    } else {
                        view.setImageResource(R.mipmap.image_placeholder);
                    }
                } else {
                    view.setImageResource(R.mipmap.image_placeholder);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "error message: " + e.getMessage());
        }
    }

}

