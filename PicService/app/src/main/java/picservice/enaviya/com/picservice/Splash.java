package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class Splash extends Activity {

    private static final String TAG = Splash.class.getSimpleName();
    private SessionManager mSessionManager;
    TextView footer;
    private static final Integer SPLASH_TIME_OUT = 3500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_splash);

            mSessionManager = new SessionManager(Splash.this);

            TextView textview = (TextView) findViewById(R.id.textview);
            footer = (TextView) findViewById(R.id.footer);

            /*final Animation animation_1 = AnimationUtils.loadAnimation(Splash.this,R.anim.rotate);
            Animation animation_2 = AnimationUtils.loadAnimation(Splash.this,R.anim.antirotate);
            final Animation animation_3 = AnimationUtils.loadAnimation(Splash.this,R.anim.abc_fade_out);*/

            PackageManager packageManager = getPackageManager();
            if (packageManager != null) {
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
                    footer.setText("Version "+packageInfo.versionName);
                    /*mSessionManager.checkLogin();
                    finish();*/
                } catch (PackageManager.NameNotFoundException e) {
                    //versionName = null;
                }
            }

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mSessionManager.checkLogin();
                    finish();
               /* Intent i = new Intent(splash.this,loginActivity.class);
                startActivity(i);
                finish();*/
                }
            }, SPLASH_TIME_OUT);

   /*         textview.startAnimation(animation_2);
            animation_2.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    textview.startAnimation(animation_1);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });*/

        /*    animation_1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    textview.startAnimation(animation_3);
                    mSessionManager.checkLogin();
                    finish();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

