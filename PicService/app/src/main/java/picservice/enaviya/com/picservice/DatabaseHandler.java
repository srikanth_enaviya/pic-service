package picservice.enaviya.com.picservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import Model.UserProfileDataModel;

/**
 * Created by user on 18-09-2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Pic Service";


    public static final String TABLE_JOB = "TableJob";
    public static final String TABLE_JOB_S_NO = "TableJob_Sno";
    public static final String TABLE_JOB_TITLE = "TableJob_title";
    public static final String TABLE_JOB_DESC = "TableJob_desc";
    public static final String TABLE_JOB_IMAGE_NAME = "TableJob_imagename";


    public static final String TABLE_GALLERY = "TableGALLERY";
    public static final String TABLE_GALLERY_SNO = "TableGallery_Sno";
    public static final String TABLE_GALLERY_IMAGE_DESC = "TableGalleryImageDesc";
    public static final String TABLE_GALLERY_IMAGE_PATH = "TableGalleryImagePath";
    public static final String TABLE_GALLERY_IMAGE_NAME = "TableGalleryImageName";
    public static final String TABLE_GALLERY_IMAGE_JOBTITLE = "TableGalleryImageJobTitle";


    public static final String TABLE_OFFLINE = "TableOFFLINE";
    public static final String OFFLINE_SNO = "TableOFF_Sno";
    public static final String OFFLINE_TITLE = "TableOFF_title";
    public static final String OFFLINE_DESC = "TableOFF_desc";
    public static final String OFFLINE_CLIENT_NAME = "TableOFF_client";
    public static final String OFFLINE_REF_NAME = "TableOFF_ref";
    public static final String OFFLINE_VESSEL_NAME = "TableOFF_vessel";
    public static final String OFFLINE_COMMODITY_NAME = "TableOFF_comm";
    public static final String OFFLINE_STOKPILE_NAME = "TableOFF_stokpile";


    public static final String TABLE_OFF_GALLERY = "TableoffGALLERY";
    public static final String TABLE_OFF_GALLERY_SNO = "TableoffGallery_Sno";
    public static final String TABLE_OFF_GALLERY_IMAGE_DESC = "TableoffGalleryImageDesc";
    public static final String TABLE_OFF_GALLERY_IMAGE_PATH = "TableoffGalleryImagePath";
    public static final String TABLE_OFF_GALLERY_IMAGE_NAME = "TableoffGalleryImageName";
    public static final String TABLE_OFF_GALLERY_IMAGE_JOBTITLE = "TableoffGalleryImageJobTitle";


    private static final String TABLE_PROFILE_IMAGE = "UserProfileImage";
    private static final String KEY_PROFILE_IMAGE_URI = "ImageURI";
    private static final String KEY_PROFILE_IMAGE_PATH = "ImagePath";
    private static final String KEY_PROFILE_IMAGE_NAME = "ImageName";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {

            String CREATE_JOB_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_JOB + "("
                    + TABLE_JOB_S_NO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TABLE_JOB_TITLE + " TEXT,"
                    + TABLE_JOB_DESC + " TEXT,"
                    + TABLE_JOB_IMAGE_NAME + " TEXT " + ")";
            db.execSQL(CREATE_JOB_TABLE);


            String CREATE_GALLERY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GALLERY + "("
                    + TABLE_GALLERY_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TABLE_GALLERY_IMAGE_DESC + " TEXT,"
                    + TABLE_GALLERY_IMAGE_PATH + " TEXT,"
                    + TABLE_GALLERY_IMAGE_NAME + " TEXT,"
                    + TABLE_GALLERY_IMAGE_JOBTITLE + " TEXT " + ")";
            db.execSQL(CREATE_GALLERY_TABLE);


            String CREATE_OFFLINE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_OFFLINE + "("
                    + OFFLINE_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + OFFLINE_TITLE + " TEXT,"
                    + OFFLINE_DESC + " TEXT,"
                    + OFFLINE_CLIENT_NAME + " TEXT,"
                    + OFFLINE_REF_NAME + " TEXT,"
                    + OFFLINE_VESSEL_NAME + " TEXT,"
                    + OFFLINE_COMMODITY_NAME + " TEXT,"
                    + OFFLINE_STOKPILE_NAME + " TEXT " + ")";
            db.execSQL(CREATE_OFFLINE_TABLE);


            String CREATE_OFF_GALLERY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_OFF_GALLERY + "("
                    + TABLE_OFF_GALLERY_SNO + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TABLE_OFF_GALLERY_IMAGE_DESC + " TEXT,"
                    + TABLE_OFF_GALLERY_IMAGE_PATH + " TEXT,"
                    + TABLE_OFF_GALLERY_IMAGE_NAME + " TEXT,"
                    + TABLE_OFF_GALLERY_IMAGE_JOBTITLE + " TEXT " + ")";
            db.execSQL(CREATE_OFF_GALLERY_TABLE);

            String CREATE_PROFILE_IMAGE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PROFILE_IMAGE + "("
                    + KEY_PROFILE_IMAGE_URI + " TEXT,"
                    + KEY_PROFILE_IMAGE_NAME + " TEXT,"
                    + KEY_PROFILE_IMAGE_PATH + " TEXT " + ")";
            db.execSQL(CREATE_PROFILE_IMAGE_TABLE);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_GALLERY);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLINE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFF_GALLERY);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE_IMAGE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static long insertintoTable(Context context, final String tableName,
                                       String[] colNames, String[] values) {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        ContentValues cv = new ContentValues();
        for (int i = 0; i < colNames.length; i++) {
            cv.put(colNames[i], values[i]);
        }
        long cnt = db.insert(tableName, null, cv);
        db.close();
        return cnt;
    }

    public static List<String[]> getQueryData(Context context, String query) {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Cursor cur = db.rawQuery(query, null);
        List<String[]> list = new ArrayList<String[]>();
        if (cur.getCount() > 0)
            list = cursortoListArr(cur);
        cur.close();
        db.close();
        dbhelper.close();
        return list;
    }

    public static List<String[]> cursortoListArr(Cursor c) {
        List<String[]> rowList = new ArrayList<String[]>();
        while (c.moveToNext()) {
            String[] arr = new String[c.getColumnCount()];
            for (int i = 0; i < c.getColumnCount(); i++) {
                arr[i] = c.getString(i);
            }
            rowList.add(arr);
        }
        c.close();
        return rowList;
    }

    public static boolean deleteRow(Context context, final String tablename,
                                    String columnName, String value) {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        boolean flag = db.delete(tablename, columnName + "='" + value + "'",
                null) > 0;
        db.close();
        return flag;
    }


    public static boolean Checkdata1(Context context, String Tablename,
                                     String fieldname,String tablename) {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Cursor res = db.query(Tablename, null,tablename+"=?",
                new String[]{fieldname}, null, null, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.close();
            return true;
        }
        db.close();
        return false;
    }

    public static boolean updateROW(Context context, final String tablename,
                                    String[] columnNames, String[] columnValues, String[] whereColumn,
                                    String[] whereValue) {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        ContentValues cv = new ContentValues();
        String Condition = "";
        for (int i = 0; i < columnNames.length; i++) {
            cv.put(columnNames[i], columnValues[i]);
        }
        for (int i = 0; i < whereColumn.length; i++) {
            if (i == whereColumn.length - 1)
                Condition += whereColumn[i] + "='" + whereValue[i] + "'";
            else
                Condition += whereColumn[i] + "='" + whereValue[i] + "' AND ";
        }
        boolean flag = db.update(tablename, cv, Condition, null) > 0;
        db.close();
        return flag;

    }



    public static boolean deleteRows(Context context, final String tablename,
                                     String columnName, String value) {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        boolean flag = db.delete(tablename, columnName + "='" + value + "'",
                null) > 0;
        db.close();
        return flag;

    }


    public void deleteTable(Context context,final String tablename)
    {
        DatabaseHandler dbhelper = new DatabaseHandler(context);
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        db.execSQL("DELETE FROM  " + tablename +";");
        System.out.println("deleted Table");
        db.close();
    }


    public void insertProfileImage(UserProfileDataModel userProfileDataModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_PROFILE_IMAGE_URI, userProfileDataModel.getImageURI());
            values.put(KEY_PROFILE_IMAGE_PATH, userProfileDataModel.getImagePath());
            values.put(KEY_PROFILE_IMAGE_NAME, userProfileDataModel.getImageName());

            String selectQuery = "SELECT  * FROM " + TABLE_PROFILE_IMAGE;
            Cursor cursor = db.rawQuery(selectQuery, null);
            int rowCount = cursor.getCount();
            cursor.close();

            if (rowCount == 0)
                db.insert(TABLE_PROFILE_IMAGE, null, values);
            else
                db.update(TABLE_PROFILE_IMAGE, values, null, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }

    public UserProfileDataModel getProfileImage() {
        SQLiteDatabase db = this.getReadableDatabase();
        UserProfileDataModel userProfileDataModel = new UserProfileDataModel();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_PROFILE_IMAGE;
            Cursor cursor;
            cursor = db.rawQuery(selectQuery, null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        userProfileDataModel.setImageURI(cursor.getString(cursor.getColumnIndex(KEY_PROFILE_IMAGE_URI)));
                        userProfileDataModel.setImagePath(cursor.getString(cursor.getColumnIndex(KEY_PROFILE_IMAGE_PATH)));
                        userProfileDataModel.setImageName(cursor.getString(cursor.getColumnIndex(KEY_PROFILE_IMAGE_NAME)));
                    }
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return userProfileDataModel;
    }

}

