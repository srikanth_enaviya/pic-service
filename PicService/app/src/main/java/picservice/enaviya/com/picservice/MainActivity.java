package picservice.enaviya.com.picservice;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import Fragments.Home;
import Fragments.ImagesView;
import Fragments.Profile;
import Fragments.Settings;
import Model.UserProfileDataModel;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final int PERMISSIONS_REQUEST_GALLERY_CODE = 101;

    NavigationView navigationView;
    ImageView imgUserPicture;
    SessionManager mSessionManager;
    ActionBar actionBar;
    DatabaseHandler db;
    DrawerLayout drawer;
    public static int navItemIndex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            //getActionBar().hide();

            mSessionManager =  new SessionManager(MainActivity.this);
            db = new DatabaseHandler(MainActivity.this);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            navigationView = (NavigationView) findViewById(R.id.nav_view);

            actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(true);
            }

            View headerLayout = navigationView.getHeaderView(0);

            TextView userName = (TextView) headerLayout.findViewById(R.id.username);
            TextView UserMailId = (TextView) headerLayout.findViewById(R.id.mailID_tv);
            imgUserPicture = (ImageView) headerLayout.findViewById(R.id.imgUserPicture);


            //userName.setText(mSessionManager.getFirstname());
            UserMailId.setText(mSessionManager.getUsername());
            Utilities.setProfilePicture(MainActivity.this, imgUserPicture);

            imgUserPicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageSelection();
                }
            });

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            drawer.setScrimColor(ContextCompat.getColor(getApplicationContext(), android.R.color.transparent));
            toggle.syncState();

            navigationView.setNavigationItemSelectedListener(this);


            if (Build.VERSION.SDK_INT >= 23)
            {
                if (checkPermission())
                {
                    // Code for above or equal 23 API Oriented Device
                    // Your Permission granted already .Do next code
                } else {
                    requestPermission(); // Code for permission
                }
            }

            navigateFragments(1, "");
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);

        }
        catch (Exception e){
            Toast.makeText(MainActivity.this, "Oops Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }


    private void imageSelection() {
        try {
            final CharSequence[] items = {"Choose from Library", "Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Add Profile Picture!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Choose from Library")) {
                        openGallery();
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            Utilities.logError(MainActivity.this, TAG, "imageSelection", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }


    private void openGallery() {
        try {
            Log.i(TAG, "openGallery");
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Image"), PERMISSIONS_REQUEST_GALLERY_CODE);
        } catch (Exception e) {
            Utilities.logError(MainActivity.this, TAG, "openGallery", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK) {
                if (requestCode == PERMISSIONS_REQUEST_GALLERY_CODE) {
                    if (data != null) {
                        Uri selectedImageUri = data.getData();
                        String uriString = selectedImageUri.toString();
                        String imageURL = getRealPathFromURI(selectedImageUri);

                        if (imageURL.isEmpty()) {
                            Utilities.showMessageAlertDialog(MainActivity.this, "Unable to get the selected image");
                        } else {
                            String imagePath = getRealPathFromURI(selectedImageUri);
                            UserProfileDataModel userProfileDataModelRequest = new UserProfileDataModel();
                            userProfileDataModelRequest.setImageName(mSessionManager.getUsername());
                            userProfileDataModelRequest.setImagePath(imagePath);
                            userProfileDataModelRequest.setImageURI(uriString);
                            db.insertProfileImage(userProfileDataModelRequest);
                            Log.i(TAG, "imagePath: " + imagePath);
                            UserProfileDataModel userProfileDataModel = db.getProfileImage();
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(userProfileDataModel.getImageURI()));
                            imgUserPicture.setImageBitmap(bitmap);
                        }
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                Utilities.showMessageAlertDialog(MainActivity.this, "Image selection has been cancelled");
            }
        } catch (Exception e) {
            Utilities.logError(MainActivity.this, TAG, "onActivityResult", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String path = "";
        try {
            String[] data = {MediaStore.Images.Media.DATA};
            Cursor cursor = (MainActivity.this).getContentResolver().query(contentUri, data, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst())
                    path = cursor.getString(column_index);
                cursor.close();
            }
        } catch (IllegalArgumentException e) {
            Utilities.logError(MainActivity.this, TAG, "getRealPathFromURI", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
        return path;
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED || result1 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(MainActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                break;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            navigateFragments(1, "");
        }
        if (id == R.id.nav_create) {
            navigateFragments(2, "create");
        } else if(id == R.id.nav_profile){
            navigateFragments(4,"");
        }
          else if (id == R.id.nav_contactUs) {
            SendMail();
        }
        else if (id == R.id.nav_settings) {
            navigateFragments(5,"");
        } else if (id == R.id.nav_logout) {
            navigateFragments(999, "");
        }

        if (item.isChecked()) {
            item.setChecked(false);
        } else {
            item.setChecked(true);
        }
        item.setChecked(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void SendMail() {
        try {
            Intent i = new Intent(Intent.ACTION_SENDTO);
            i.setData(Uri.parse("mailto:" + "yogesh_26@hotmail.com"));
            i.putExtra(Intent.EXTRA_SUBJECT, "Pic Service");
            startActivity(Intent.createChooser(i, "Send mail"));
        }
        catch (Exception e){
            Log.e("SendMail",e.getMessage());
        }
    }

    public void navigateFragments(Integer position, String information) {
        Fragment fragment = null;
        String FRAGMENT_TAG = "";
        Bundle bundle = new Bundle();
        try {
            switch (position) {
                case 1:
                    actionBar.setTitle("Home");
                    fragment = new Home();
                    FRAGMENT_TAG = Home.class.getSimpleName();
                    bundle.clear();
                    bundle.putString("Information", information);
                    fragment.setArguments(bundle);
                    break;
                case 2:
                    fragment = new Home();
                    FRAGMENT_TAG = Home.class.getSimpleName();
                    bundle.clear();
                    bundle.putString("Information", information);
                    fragment.setArguments(bundle);
                    break;
                case 3:
                    actionBar.setTitle(information);
                    fragment = new ImagesView();
                    FRAGMENT_TAG = ImagesView.class.getSimpleName();
                    bundle.clear();
                    bundle.putString("Information", information);
                    fragment.setArguments(bundle);
                    break;
                case 4:
                    actionBar.setTitle("Profile");
                    fragment = new Profile();
                    FRAGMENT_TAG = Profile.class.getSimpleName();
                    bundle.clear();
                    bundle.putString("Information", information);
                    fragment.setArguments(bundle);
                    /*infoDialog InfoDialog = new infoDialog();
                    InfoDialog.show(getFragmentManager(),
                            "Confirmation dialog");
                    //fragment = new Home();
                    FRAGMENT_TAG = "infodialog";
                    //bundle.clear();
                    //bundle.putString("Information", information);
                    //fragment.setArguments(bundle);*/
                    break;
                case 5:
                    actionBar.setTitle("Settings");
                    fragment = new Settings();
                    FRAGMENT_TAG = Settings.class.getSimpleName();
                    bundle.clear();
                    bundle.putString("Information", information);
                    fragment.setArguments(bundle);
                    break;
                case 999:
                    ShowLogoutDialog();
                    //FRAGMENT_TAG = SignIn.class.getSimpleName();
                    break;
            }

            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment,FRAGMENT_TAG).commit();

                // update selected item and title, then close the drawer
            } else {
                // error in creating fragment
                Log.e("MainActivity", "Error in creating fragment");
            }
        } catch (Exception e) {
            Utilities.logError(MainActivity.this, TAG, "navigateFragments", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }


    private void ShowLogoutDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                MainActivity.this);
        // set title
        alertDialogBuilder.setTitle("Logout");
        // set dialog message
        alertDialogBuilder
                .setMessage("Are you sure you want to logout ?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mSessionManager.LogoutUser();
                finish();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    int TIME_INTERVAL = 2000;
    long mBackPressed;

    @Override
    public void onBackPressed() {
        try {
            Home homefragment = (Home) getSupportFragmentManager().findFragmentByTag(Home.class.getSimpleName());
            ImagesView IVfragment = (ImagesView) getSupportFragmentManager().findFragmentByTag(ImagesView.class.getSimpleName());
            Profile profilefragment = (Profile) getSupportFragmentManager().findFragmentByTag(Profile.class.getSimpleName());
            Settings settingsfragment = (Settings) getSupportFragmentManager().findFragmentByTag(Settings.class.getSimpleName());

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer != null) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    if(homefragment != null && homefragment.isVisible()) {
                        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                            super.onBackPressed();
                            return;
                        } else {
                            Toast.makeText(MainActivity.this, "Tap back button again to exit", Toast.LENGTH_SHORT).show();
                        }
                        mBackPressed = System.currentTimeMillis();
                    } else
                    if(IVfragment != null && IVfragment.isVisible()){
                        navigateFragments(1, "");
                    } else if(profilefragment != null && profilefragment.isVisible()){
                        navigateFragments(1, "");
                    }  else if(settingsfragment != null && settingsfragment.isVisible()){
                        navigateFragments(1, "");
                    } else {
                        super.onBackPressed();
                    }
                }
            }
        } catch (Exception e) {
            Utilities.logError(MainActivity.this, TAG, "onBackPressed", e.getMessage(), getResources().getString(R.string.something_went_wrong));
        }
    }
}
