package picservice.enaviya.com.picservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;

import Adapters.FullScreenImageAdapter;
import Fragments.ImagesView;

public class FullScreenViewActivity extends Activity{

    private Utils utils;
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_full_screen_view);

            viewPager = (ViewPager) findViewById(R.id.pager);

            utils = new Utils(getApplicationContext());

            Intent i = getIntent();
            String position = i.getStringExtra("position");
            //int position = i.getIntExtra("position", 0);

            adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
                    ImagesView.Image_Uri);

            viewPager.setAdapter(adapter);

            // displaying selected image first
            viewPager.setCurrentItem(Integer.parseInt(position));
        } catch (Exception e){
            Log.e("Excep ", e.getMessage());
        }
    }
}

