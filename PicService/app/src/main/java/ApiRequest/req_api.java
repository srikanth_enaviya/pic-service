package ApiRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 18-09-2017.
 */
public class req_api {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("contenttype")
    @Expose
    private String contenttype;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("idProject")
    @Expose
    private IdProject idProject;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public IdProject getIdProject() {
        return idProject;
    }

    public void setIdProject(IdProject idProject) {
        this.idProject = idProject;
    }




    public class IdProject {

        @SerializedName("projectTitle")
        @Expose
        private String projectTitle;

        public String getProjectTitle() {
            return projectTitle;
        }

        public void setProjectTitle(String projectTitle) {
            this.projectTitle = projectTitle;
        }

    }


}



